/*
EquiLeader

Find the index S such that the leaders of the sequences A[0], A[1],
..., A[S] and A[S + 1], A[S + 2], ..., A[N - 1] are the same.

A non-empty zero-indexed array A consisting of N integers is given.

The leader of this array is the value that occurs in more than half of
the elements of A.

An equi leader is an index S such that 0 ≤ S < N − 1 and two sequences
A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N − 1] have
leaders of the same value.

For example, given array A such that:

    A[0] = 4
    A[1] = 3
    A[2] = 4
    A[3] = 4
    A[4] = 4
    A[5] = 2
we can find two equi leaders:

0, because sequences: (4) and (3, 4, 4, 4, 2) have the same leader,
whose value is 4.  2, because sequences: (4, 3, 4) and (4, 4, 2) have
the same leader, whose value is 4.  The goal is to count the number of
equi leaders.

Write a function:

int solution(int A[], int N);
that, given a non-empty zero-indexed array A consisting of N integers,
returns the number of equi leaders.

For example, given:

    A[0] = 4
    A[1] = 3
    A[2] = 4
    A[3] = 4
    A[4] = 4
    A[5] = 2
the function should return 2, as explained above.

Assume that:

N is an integer within the range [1..100,000]; each element of array A
is an integer within the range [−1,000,000,000..1,000,000,000].

Complexity:

expected worst-case time complexity is O(N); expected worst-case space
complexity is O(N), beyond input storage (not counting the storage
required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/


#include<iterator>
#include<vector>

using namespace std;
 

// vector<int> fillMy(vector<int>::const_iterator first,
// 			vector<int>::const_iterator last)
// {

//   int N = distance(first, last);

//   for (auto it = first ; it != last ; it++) {



//   }

  
//   vector<int> result(5);

//   return result;
  

// }




// template<typename Iter>
// vector<int> getCandidatesArray(Iter begin, Iter end) {

//   int candidate, candidateCnt;
//   vector<int> result;
  
//   if (begin == end)
//     return result; // vector<int> (0)?

//   candidateCnt = 0;
  
//   for(auto it = begin; it != end; ++it) {

//     if (candidateCnt == 0) {
//       candidate = *it;
//       candidateCnt = 1;
//     }
//     else if (*it == candidate) candidateCnt++;
//     else candidateCnt--;

//   result.push_back(candidate);
//   }

// return result;

// }



// vector<int> getCandidatesForward(const vector<int> &A) {

//   return getCandidatesArray(A.cbegin(), A.cend());

// }

// vector<int> getCandidatesBackward(const vector<int> &A) {

//   return getCandidatesArray(A.crbegin(), A.crend());

// }


// template<typename Iter>
// bool isLeader(int candidate, Iter begin, Iter end) {

//   int size = distance(begin, end);
//   int cnt = 0;

//   for(; begin != end; ++begin)
//     if (*begin == candidate) cnt++;

  
//   return cnt > size / 2.0; 
// }



//Assume: A is nonempty
int getLeaderCandidate(const vector<int> &A) {

    int candidate, candidateCnt;
  
  candidateCnt = 0;
  
  for(int v: A) {

    if (candidateCnt == 0) {
      candidate = v;
      candidateCnt = 1;
    }
    else if (v == candidate) candidateCnt++;
    else candidateCnt--;
  }

return candidate;

}




template<typename Iter>
vector<int> getOccurrencesOf(int v, Iter begin, Iter end) {

  vector<int> result;
  int vCnt = 0;

  for(; begin != end; ++begin) {
    if (*begin == v) vCnt++;
    result.push_back(vCnt);
  }

  return result;
}


vector<int> getOccurrencesOfForward(int v, const vector<int> &A) {

  return getOccurrencesOf(v, A.cbegin(), A.cend());
}

vector<int> getOccurrencesOfBackward(int v, const vector<int> &A) {

  return getOccurrencesOf(v, A.crbegin(), A.crend());
}


template<typename Iter>
bool isLeader(int candidate, Iter begin, Iter end) {

  int size = distance(begin, end);
  int cnt = 0;

  for(; begin != end; ++begin)
    if (*begin == candidate) cnt++;

  
  return cnt > size / 2.0; 
}


 int solution(vector<int> &A) {
   // write your code in C++11 (g++ 4.8.2)


   
   //vector<int> candidatesForward = getCandidatesForward(A);
   //vector<int> candidatesBackward = getCandidatesBackward(A);
   

   //The only possible value of an equileader is the value of leader.
   //Therefore we must first check if A has a leader and if yes -- compute
   // its value.

   int N = A.size();
  
    int leaderCandidate = getLeaderCandidate(A);

    vector<int> occurrencesForward =
      getOccurrencesOfForward(leaderCandidate, A);
    vector<int> occurrencesBackward =
      getOccurrencesOfBackward(leaderCandidate, A);

    
    if (occurrencesForward[N-1] <= N / 2.0)
       return 0; // no leaders, no equleaders

    
   // We will now compute the number of equileaders.
     int leader = leaderCandidate;
     int result = 0;

     for(int i = 0; i < N-1; i++) {

       //TODO: This is too low level. Refactor it!
       if ( (occurrencesForward[i] > (i + 1) / 2.0) &&
	    (occurrencesBackward[N - i - 2] > (N - i - 1) / 2.0)
	    ) {
	 result++;
       }
       
     }
          
   return result;
}


