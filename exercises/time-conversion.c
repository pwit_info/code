/*
Problem Statement

You are given time in AM/PM format. Can you convert this into a 24-hour format?

Input

Input consists of time in the AM/PM format i.e. hh:mm:ssAM or hh:mm:ssPM 
where 01≤hh≤12.

Sample: 07:05:45PM

Output

You need to print the time in a 24-hour format i.e. hh:mm:ss 
where 00≤hh≤23.

Sample output for the above input: 19:05:45

Note: Midnight is 12:00:00AM or 00:00:00. Noon is 12:00:00PM.
*/


#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    unsigned char hours, minutes, seconds;
    char A_or_P;
    scanf("%hhu:%hhu:%hhu%c",&hours,&minutes,&seconds,&A_or_P);
   
    if ( (A_or_P == 'P') && (hours != 12) ) {
        hours += 12;
    }
    
    if ( (A_or_P == 'A') && (hours == 12)) {
        hours = 0;
    }
    
    
    printf("%02hhu:%02hhu:%02hhu",hours,minutes,seconds);
    
    return 0;
}


