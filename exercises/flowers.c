/*
Problem Statement

You and your K−1 friends want to buy N flowers. Flower number i has
cost ci. Unfortunately the seller does not want just one customer to
buy a lot of flowers, so he tries to change the price of flowers for
customers who have already bought some flowers. More precisely, if a
customer has already bought x flowers, he should pay (x+1)ci dollars
to buy flower number i.  You and your K−1 friends want to buy all N
flowers in such a way that you spend the least amount of money. You
can buy the flowers in any order.

Input:

The first line of input contains two integers N and K(K<=N). The next
line contains N space separated positive integers c1,c2,...,cN.

Output:

Print the minimum amount of money you (and your friends) have to pay
in order to buy all N flowers.

Constraints

1≤N,K≤100 
Any ci is not more than 106 
Result is guaranteed to be less than 231

Sample input #00

3 3
2 5 6
Sample output #00

13
Sample input #01

3 2
2 5 6
Sample output #01

15 Explanation : Sample Case #00: In this example, all of you should
buy one flower each. Hence, you'll have to pay 13 dollars.  Sample
Case #01: Here one of the friend buys first two flowers in decreasing
order of their price. So he will pay (0+1)*5 + (1+1)*2 = 9. And other
friend will buy the costliest flower of cost 6. So total money need is
9+6=15.

Copyright © 2015 HackerRank.  All Rights Reserved Related Topics
Greedy Technique Sorting Submissions: 11813 Max Score: 35 Difficulty:
Moderate
*/

/* Sample program illustrating input/output methods */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>


typedef unsigned int entry;


entry *tmp_arr;


void mergesort_rec(entry *arr, entry *buff, unsigned left, unsigned right) {
    
    
    if (left == right)
        return;
    
    /* left < right*/
    
    unsigned mid = (left + right) / 2;
    
    mergesort_rec(buff, arr, left, mid);
    mergesort_rec(buff, arr, mid+1, right);
    
    /* merge*/
    
    unsigned la = left, lb = mid + 1, i = left;
    
    while ( (la <= mid) && (lb <= right) ) {
        
        if ( buff[la] <= buff[lb]) {
            arr[i++] = buff[la++]; 
        }
        else {
            arr[i++] = buff[lb++];
        }
    }
        
    while ( la <= mid )
        arr[i++] = buff[la++];
    while ( lb <= right)
        arr[i++] = buff[lb++];
                
     
          
}

void mergesort(entry *arr, unsigned N) {
    
    tmp_arr = malloc (N * sizeof(entry));
    
    memcpy(tmp_arr, arr, N * sizeof(entry));
    mergesort_rec(arr, tmp_arr, 0, N-1);
    
    free(tmp_arr);
}





int main(){

//Helpers for input/output
   int i, N, K;
   unsigned *C;
   
   scanf("%d %d", &N, &K);
    
    C = malloc(N*sizeof(unsigned));
       
    for(i=0; i<N; i++){
      scanf("%u", &(C[i]));
   }
    
    mergesort(C, N);
    
    /* C is now sorted in non-descending order*/
    unsigned result = 0;
    unsigned flowers_bought;
    
    for (int buyer = 0; buyer < K; buyer++) {
        
        flowers_bought = 0 ;
        
        for ( int flower = N - 1 - buyer; flower >= 0; flower -= K) {
            
         result += (flowers_bought + 1) * C[flower];      
         flowers_bought++;   
            
        }                       
    }
   
    
    
    printf("%u\n", result);

    free(C);
}
