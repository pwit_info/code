/*
Problem Statement

You are given a square matrix of size N×N. Can you calculate the absolute difference of the sums across the two main diagonals?

Input Format

The first line contains a single integer N. The next N lines contain the rows of N integers describing the matrix.

Output Format

Output a single integer equal to the absolute difference in the sums across the diagonals.

Sample Input

3
11 2 4
4 5 6
10 8 -12
Sample Output

15
Explanation

The first diagonal of the matrix is:

11
    5
        -12
Sum across the first diagonal: 11 + 5 - 12 = 4

The second diagonal of the matrix is:

        4
    5
10
Sum across the second diagonal: 4 + 5 + 10 = 19 
Difference: |4 - 19| = 15

Copyright © 2015 HackerRank.
All Rights Reserved
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    
    unsigned int N;
    long left_diag = 0, right_diag = 0;
    int current_value;
    
    scanf("%u", &N);
    
    for (unsigned int i = 1; i <= N; i++)
      for (unsigned int j = 1; j <= N; j++) {
	
	scanf("%d", &current_value);
	if (j == i) left_diag += current_value;
	if (N-j+1 == i) right_diag += current_value;        
      }
    
    printf("%ld", labs(left_diag - right_diag));
    
    return 0;
}
