#include<stdio.h>



int main(void) {

  short int si = 0x0102;

  printf("Testing short int placement\n");
  printf("Byte at &si is %x\n", *((char *)(&si)));
  printf("Byte at &si+1 is %x\n", *((char *)(&si)+1));
  

  int i = 0x01020304;

  printf("Testing int placement\n");
  printf("Byte at &i is %x\n", *((char *)(&i)));
  printf("Byte at &i+1 is %x\n", *((char *)(&i)+1));
  printf("Byte at &i+2 is %x\n", *((char *)(&i)+2));
  printf("Byte at &i+3 is %x\n", *((char *)(&i)+3));
 
  



  return 0;
}
