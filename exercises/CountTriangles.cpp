/*
CountTriangles

Count the number of triangles that can be built from a given set of
edges.  Task description A zero-indexed array A consisting of N
integers is given. A triplet (P, Q, R) is triangular if it is possible
to build a triangle with sides of lengths A[P], A[Q] and A[R]. In
other words, triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:

A[P] + A[Q] > A[R],
A[Q] + A[R] > A[P],
A[R] + A[P] > A[Q].

For example, consider array A such that:

  A[0] = 10    A[1] = 2    A[2] = 5
  A[3] = 1     A[4] = 8    A[5] = 12

There are four triangular triplets that can be constructed from
elements of this array, namely (0, 2, 4), (0, 2, 5), (0, 4, 5), and
(2, 4, 5).

Write a function:

int solution(vector<int> &A);

that, given a zero-indexed array A consisting of N integers, returns
the number of triangular triplets in this array.

For example, given array A such that:

  A[0] = 10    A[1] = 2    A[2] = 5
  A[3] = 1     A[4] = 8    A[5] = 12
the function should return 4, as explained above.

Assume that:

N is an integer within the range [0..1,000];
each element of array A is an integer within the range [1..1,000,000,000].
Complexity:

expected worst-case time complexity is O(N2); expected worst-case
space complexity is O(N), beyond input storage (not counting the
storage required for input arguments).  Elements of input arrays can
be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/

//Remark: Better than expected space complexity: O(1)


// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;


#include<algorithm>
#include<iostream>

int solution(vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)

int N = A.size();
int result = 0;

if (N < 3)
  return 0;
  
sort(A.begin(), A.end());

//cout << A[0] << " " << A[1] << " " << A[2] << " " << A[3] << " " << A[4] << " " << A[5];
 
int l = 0, s = 1, r = 2;  
  
while (l < N-2) {
    
 while ((r < N) && (A[l] + A[s] > A[r])) {
   r++;
 }    
 
 result+= r-s-1;
 

 if (s < N - 2) {     
     s++;
     if (r == s) r = s+1;  
 } else 
 { 
   l++;
   s = l+1;
   r = s+1;
 }   

}
  
  return result;
}


