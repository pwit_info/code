/*
Problem Statement

Your teacher has given you the task of drawing a staircase
structure. Being an expert programmer, you decided to make a program
to draw it for you instead. Given the required height, can you print a
staircase as shown in the example?
Input You are given an integer N depicting the height of the
staircase.

Output Print a staircase of height N that consists of # symbols and
spaces. For example for N=6, here's a staircase of that height:

     #
    ##
   ###
  ####
 #####
######
Note: The last line has 0 spaces before it.
*/


#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {

    unsigned int N;
    
    scanf("%u", &N);
    
    for (unsigned int row = 1; row <= N; row++ ) {
        for( unsigned int col = 1; col <= N; col++) {
        
        if (col > N - row)
            putchar('#');
        else
            putchar(' ');
           
        }   
     putchar('\n');
    }
    
    
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */    
    return 0;
}
