/*
1. Dominator Find an index of an array such that its value occurs at
more than half of indices in the array.

Task description

A zero-indexed array A consisting of N integers is given. The
dominator of array A is the value that occurs in more than half of the
elements of A.

For example, consider array A such that

 A[0] = 3    A[1] = 4    A[2] =  3
 A[3] = 2    A[4] = 3    A[5] = -1
 A[6] = 3    A[7] = 3

The dominator of A is 3 because it occurs in 5 out of 8 elements of A
(namely in those with indices 0, 2, 4, 6 and 7) and 5 is more than a
half of 8.

Write a function

int solution(vector<int> &A);
that, given a zero-indexed array A consisting of N integers, returns
index of any element of array A in which the dominator of A
occurs. The function should return −1 if array A does not have a
dominator.

Assume that:

N is an integer within the range [0..100,000]; each element of array A
is an integer within the range [−2,147,483,648..2,147,483,647].  

For example, given array A such that

 A[0] = 3    A[1] = 4    A[2] =  3
 A[3] = 2    A[4] = 3    A[5] = -1
 A[6] = 3    A[7] = 3
the function may return 0, 2, 4, 6 or 7, as explained above.

Complexity:

expected worst-case time complexity is O(N); expected worst-case space
complexity is O(1), beyond input storage (not counting the storage
required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/

// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;


//Assume:   A.size() > 0;
//Guarantee: candidate for a leader is selected and returned
int findCandidate(const vector<int> &A) {
 
 int N = A.size();
 int candidate = A[0];
 int count = 1;
 
 for (int i = 1; i < N; i++)  {
     
     if ( candidate == A[i])
       count++;
     else
       if( count == 0) {
         candidate = A[i];
         count = 1; 
       }
     else
        count--;          
 }
       
  return candidate;
}


int solution(vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)

int N = A.size();

if (N == 0)
  return -1;
  
int candidate = findCandidate(A);  
int count = 0, index;

//verification of the candidate
for(int i = N-1; i >= 0; i--) {
   if (A[i] == candidate) {
    count++;   
    index = i;   
   }
}

  if (count > N / 2)
    return index;
  else
    return -1;
}

