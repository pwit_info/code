
#include<stdexcept>
#include<vector>
#include<iostream>
#include<queue>
#include "roundbuffer.hpp"


template <typename T>
RoundBuffer<T>::RoundBuffer(size_t capacity)
  : capacity(capacity), left(0), right(0), isEmpty(true)
{

  array = new T[capacity]; //may throw 
}

template <typename T>
RoundBuffer<T>::RoundBuffer(typename std::vector<T>::iterator begin,
			    typename std::vector<T>::iterator  end)
{

  capacity = distance(begin, end);
  ///  capacity = end - begin + 1;
  array = new T[capacity]; //may throw 
  left = right = 0;
  isEmpty = true;

  copy(begin, end, array);   // uniterpreted_copy ??

}


template <typename T>
RoundBuffer<T>::RoundBuffer(const RoundBuffer<T> &src) {

  capacity = src.capacity;
  left = src.left;
  right = src.right;
  isEmpty = src.isEmpty;
  array = new T[capacity];

  for(size_t i = 0; i < capacity; i++) {

    array[i] = src.array[i]; //not opt
  }
    
  
}


template <typename T>
RoundBuffer<T>::~RoundBuffer() {

  delete[] array; //??

}


template <typename T>
bool RoundBuffer<T>::empty() const noexcept {
   return isEmpty;
}

template <typename T>
size_t RoundBuffer<T>::size() const noexcept {

  if (isFull()) return capacity;
  return (right - left + capacity) % capacity;

  //return capacity;
}


template <typename T>
T& RoundBuffer<T>::front() {
  if (isEmpty)
    throw std::length_error("Round Buffer is empty. Operation front() failed.");

  return array[left];

}

template <typename T>
const T& RoundBuffer<T>::front() const {

  return const_cast<RoundBuffer<T>*>(this)->front();
}

//[left,right)

template <typename T>
T& RoundBuffer<T>::back() {

  if (isEmpty)
    throw std::length_error("Round Buffer is empty. Operation back() failed.");
 
  return array[ (right + capacity - 1) % capacity];
}

template <typename T>
const T& RoundBuffer<T>::back() const {

  return const_cast<RoundBuffer<T>*>(this)->back();
}


template <typename T>
void RoundBuffer<T>::push_back(const T& elem) {

  if (isFull())
    throw std::length_error("Round Buffer is full. Operation push_back failed.");

  isEmpty = false;
  array[right] = elem;
  
  right = (right + 1) % capacity;
  
}

template <typename T>
void RoundBuffer<T>::push_back(T&& elem) {

  if (isFull())
    throw std::length_error("Round Buffer is full. Operation push_back failed.");

  isEmpty = false;
  std::swap(array[right], elem); 
  right = (right + 1) % capacity;
   
}


template <typename T>
void RoundBuffer<T>::pop_front() {

  if (isEmpty)
    throw std::length_error("Round Buffer is empty. Operation pop_front failed.");

  left = (left + 1) % capacity;
  if (left == right) isEmpty = true;
  

}

template <typename T>
bool RoundBuffer<T>::isFull() const noexcept {

  return (!isEmpty) && (left == right);
  
}


template<typename T>
std::ostream& operator<<(std::ostream &os, const RoundBuffer<T> &rb) {

  if (!rb.empty()) {
      size_t i   = rb.left;
      do {
	  if (i != rb.left) os<<", ";
	  os << rb.array[i];
          i = (i + 1) % rb.capacity;
      } while (i!=rb.right);
  }
  
  return os;
}

template <typename T, typename C>
std::ostream& operator<<(std::ostream &os, std::queue<T,C> &q) {

  size_t size = q.size();

  for(size_t i = 0; i < size; i++) {

    T elem = q.front(); // reference? why not?
    q.pop();
    if (i > 0) os << ", ";
    os << elem;
    q.push(elem);
  }
  
  return os;

}




template class RoundBuffer<int>;
template std::ostream& operator<<(std::ostream&,
				  const RoundBuffer<int> &);

//template class std::queue<int,RoundBuffer<int>>;
template std::ostream& operator<<(std::ostream&,
				  std::queue<int,RoundBuffer<int>>&);






