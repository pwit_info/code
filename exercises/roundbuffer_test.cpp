#include "roundbuffer.hpp"

using namespace std;

#include<stdexcept>
#include<vector>
#include<iostream>

#include<queue>

#include "roundbuffer.hpp"

#include<iostream>


int main(void) {

    
  RoundBuffer<int> rb(5);

  
  rb.push_back(0);
  rb.push_back(1);
  rb.push_back(2);
  rb.push_back(3);
  rb.push_back(4);
  //rb.push_back(5);

  cout << rb << endl;
  rb.pop_front();
  rb.push_back(5);

  cout << rb << endl;
  
  rb.pop_front();   
  rb.pop_front();   
  rb.pop_front();   
  rb.pop_front();   
  cout << rb << endl;
  rb.pop_front();   
  cout << rb << endl;
  rb.push_back(3);
  cout << rb << endl << "----" <<  endl;

 
  // //RoundBuffer<int> rb(5);

  queue<int,RoundBuffer<int>> q(5);

  q.push(0);
  q.push(1);
  q.push(2);
  q.push(3);
  q.push(4);
  // q.push(5);

   cout << q << endl;
   cout << q << endl;

  q.pop();
  q.push(5);

  cout << q << endl;
  
  q.pop();
  q.pop();
  q.pop();
  q.pop();
  
  cout << q << endl;
  q.pop();   
  cout << q << endl;
  q.push(3);
  cout << q << endl;

  
  
  return 0;
}
