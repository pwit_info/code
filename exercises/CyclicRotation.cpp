/*
1. CyclicRotation Rotate an array to the right by a given number of
steps.

Task description A zero-indexed array A consisting of N integers is
given. Rotation of the array means that each element is shifted right
by one index, and the last element of the array is also moved to the
first place.

For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9,
7]. The goal is to rotate array A K times; that is, each element of A
will be shifted to the right by K indexes.

Write a function:

vector<int> solution(vector<int> &A, int K); that, given a
zero-indexed array A consisting of N integers and an integer K,
returns the array A rotated K times.

For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function
should return [9, 7, 6, 3, 8].

Assume that:

N and K are integers within the range [0..100]; each element of array
A is an integer within the range [−1,000..1,000].  In your solution,
focus on correctness. The performance of your solution will not be the
focus of the assessment.  

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/



// you can use includes, for example:
// #include <algorithm>

#include<vector>


// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;


//pwit: very inefficient solution: multiple copies of vectors created, 
//      multiple rotations by one instead of one rotation by K.
//      But the authors told us to ignore performance issues.

using namespace std;

vector<int> RotateByOne(vector<int> &A) {
 
 size_t N = A.size();
 vector<int> result(N);   
    
 result[0] = A[N-1];
 
 for( size_t cnt = N-1; cnt > 0; cnt --) {
  
  result[cnt] = A[cnt-1];   
 }
    
    return result;
}

vector<int> solution(vector<int> &A, int K) {
    // write your code in C++11 (g++ 4.8.2)
    
    vector<int> result = A;
    
    for (int cnt = 0; cnt < K; cnt++) {
        
        result = RotateByOne(result);
        
    }
    
    return result;
}


