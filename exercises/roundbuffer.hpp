#ifndef ROUNDBUFFER_HPP
#define ROUNDBUFFER_HPP
#include<iostream>
#include<vector>
#include<queue>

template <typename T>
class RoundBuffer {

  //  using value_type = T;  
  
  T* array;
  size_t capacity; // assert capacity >= 1;
  size_t left, right;
  bool isEmpty;
  
public:

  //  typedef T value_type;
  using value_type = T;
  using reference = T&;
  using const_reference = const T&;
  using size_type = size_t;
  
  
  RoundBuffer(size_t capacity);
  RoundBuffer(typename std::vector<T>::iterator,
	      typename std::vector<T>::iterator);
  RoundBuffer(const RoundBuffer<T> &);

  
  ~RoundBuffer();

  bool empty(void) const noexcept;
  size_t size() const noexcept;
  T& front();
  const  T& front() const;
  T& back();
  const  T& back() const;
  void push_back(const T &);
  void push_back(T &&);
  void pop_front();

private:
  inline bool isFull() const noexcept;

  template<typename U>
  friend std::ostream& operator<<(std::ostream &os, const RoundBuffer<U> &rb);  
};


template <typename T, typename C>
extern std::ostream& operator<<(std::ostream &os, std::queue<T,C> &q);

#endif
