/*

CountSemiprimes

Count the semiprime numbers in the given range [a..b] Task description
A prime is a positive integer X that has exactly two distinct
divisors: 1 and X. The first few prime integers are 2, 3, 5, 7, 11 and
13.

A semiprime is a natural number that is the product of two (not
necessarily distinct) prime numbers. The first few semiprimes are 4,
6, 9, 10, 14, 15, 21, 22, 25, 26.

You are given two non-empty zero-indexed arrays P and Q, each
consisting of M integers. These arrays represent queries about the
number of semiprimes within specified ranges.

Query K requires you to find the number of semiprimes within the range
(P[K], Q[K]), where 1 ≤ P[K] ≤ Q[K] ≤ N.

For example, consider an integer N = 26 and arrays P, Q such that:

    P[0] = 1    Q[0] = 26
    P[1] = 4    Q[1] = 10
    P[2] = 16   Q[2] = 20
The number of semiprimes within each of these ranges is as follows:

(1, 26) is 10,
(4, 10) is 4,
(16, 20) is 0.

Write a function:

vector<int> solution(int N, vector<int> &P, vector<int> &Q);

that, given an integer N and two non-empty zero-indexed arrays P and Q
consisting of M integers, returns an array consisting of M elements
specifying the consecutive answers to all the queries.

For example, given an integer N = 26 and arrays P, Q such that:

    P[0] = 1    Q[0] = 26
    P[1] = 4    Q[1] = 10
    P[2] = 16   Q[2] = 20
the function should return the values [10, 4, 0], as explained above.

Assume that:

N is an integer within the range [1..50,000];
M is an integer within the range [1..30,000];
each element of arrays P, Q is an integer within the range [1..N];
P[i] ≤ Q[i].
Complexity:

expected worst-case time complexity is O(N*log(log(N))+M); expected
worst-case space complexity is O(N+M), beyond input storage (not
counting the storage required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/

// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;


constexpr int UNTOUCHED = 0;
constexpr int FOUND = 1;

//checks range 0..N-1
vector<int> prepareRawSieve(int N) {
       
vector<int> sieve(N, UNTOUCHED);

  for(int p = 2; p < N; p++) {      
   if (sieve[p] == UNTOUCHED) { //sieve[p] is prime   
      for(int j = 2*p; j < N; j+=p) {
      //sieve[j] is a prime, a semiprime or a composite number 
       
        if(sieve[j] == UNTOUCHED) {
         if( j == p*p)
           sieve[j] = FOUND;
         else  // p divides j;
           sieve[j] = j / p ;        
        }
        else 
            if( sieve[j] == p)
              sieve[j] = FOUND;               
   }
}
}
    
 return sieve;   
}



vector<int> prepareRange(int right) {
    
vector<int> result = prepareRawSieve(right+1);

for(int i = 2; i<= right; i++) {    
    result[i] = result[i-1] + static_cast<int>((result[i] == FOUND));
}
  return result;
}


vector<int> solution(int N, vector<int> &P, vector<int> &Q) {
    // write your code in C++11 (g++ 4.8.2)


vector<int> sieve = prepareRange(N);

/*
for(int i =0; i < sieve.size(); i++) {
 
 cout << sieve[i] << " ";   
}
*/


int M = P.size();

vector<int> result(M);
for(int i = 0; i < M; i++) {
    result[i] = sieve[Q[i]] - sieve[P[i]-1];    
}

 return result;
}



