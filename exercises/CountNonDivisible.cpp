/*
1. CountNonDivisible

Calculate the number of elements of an array that are not divisors of
each element. 

Task description 

You are given a non-empty zero-indexed array A consisting of N
integers.

For each number A[i] such that 0 ≤ i < N, we want to count the number
of elements of the array that are not the divisors of A[i]. We say
that these elements are non-divisors.

For example, consider integer N = 5 and array A such that:

    A[0] = 3
    A[1] = 1
    A[2] = 2
    A[3] = 3
    A[4] = 6
For the following elements:

A[0] = 3, the non-divisors are: 2, 6,
A[1] = 1, the non-divisors are: 3, 2, 3, 6,
A[2] = 2, the non-divisors are: 3, 3, 6,
A[3] = 3, the non-divisors are: 2, 6,
A[4] = 6, there aren't any non-divisors.
Write a function:

vector<int> solution(vector<int> &A);

that, given a non-empty zero-indexed array A consisting of N integers,
returns a sequence of integers representing the amount of
non-divisors.

The sequence should be returned as:

a structure Results (in C), or
a vector of integers (in C++), or
a record Results (in Pascal), or
an array of integers (in any other programming language).
For example, given:

    A[0] = 3
    A[1] = 1
    A[2] = 2
    A[3] = 3
    A[4] = 6
the function should return [2, 4, 3, 2, 0], as explained above.

Assume that:

N is an integer within the range [1..50,000]; each element of array A
is an integer within the range [1..2 * N].

Complexity:

expected worst-case time complexity is O(N*log(N)); expected
worst-case space complexity is O(N), beyond input storage (not
counting the storage required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/



// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

vector<int> solution(vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)
    
int N = A.size();
const int TN = 2*N+1;

//Intention: sieve[i] -> # of occurences of i in A
vector<int> sieve(TN,0);
//Intention: divisors[i] -> the number of divisors of i contained in A
vector<int> divisors(TN,0);


for(int v: A) sieve[v]++;

//Compute divisors, the main task of this program
for(int i = 0; i < TN; i++) {
    
    if (sieve[i]) {
        for(int j = i; j < TN; j+=i) {

            divisors[j] += sieve[i];
        }        
    }
}

vector<int> result(N,0);


for(int i =0; i< N; i++) {
    result[i] = N - divisors[A[i]];
}

 return result;   
    
}


