////////////////////////////////////////////////////////////////////////////////
// Exercise: zad1liczbyrzymskie.pdf. Solution by pwit.
////////////////////////////////////////////////////////////////////////////////


#include<cstring>
#include<cctype>
#include<string>
#include<iostream>

using std::string;
using std::cout;
using std::cerr;


// Assumption: x is a c-string
// Result: true iff x represents a number in range 1 -- 3999.
//         No leading 0s allowed.
bool correct(const char *x) {

  if ( (x == nullptr) || (strlen(x) > 4) || (x[0] == '0') ||
       ( (x[0] > '3') && (strlen(x) >= 4)))
    return false;

  for (int i = 0; i < strlen(x); i++) {
    if ( !isdigit( x[i]))
      return false;
  }
  return true;
}

int arab_bin(const char *x) {

  if ( !correct(x)) {
    cerr << "String \"" << x <<"\" does not represent valid number 1..3999. ";
    return 0;
  }
  return atoi(x);
}

std::string bin_rzym(int x) {

  const int B[] = {1000, 900, 500, 400, 100, 90,
		   50, 40, 10, 9, 5, 4, 1};
  const std::string R[] = {"M", "CM", "D", "CD", "C",
			   "XC", "L", "XL", "X",
			   "IX", "V", "IV", "I"};

  string result;

  for (int i = 0; i < sizeof(B) / sizeof(int); i++) {
    while (x >= B[i]) {
      result += R[i];
      x -= B[i];
    }
  }

  return result;
}



int main(int argc, char* argv[]) {

  
  for (int argnr = 1; argnr < argc; argnr++) {

    int x = arab_bin(argv[argnr]);

    if (x == 0)
      cerr << "Argument error\n";
    else
      cout << bin_rzym(x) << "\n";
    
  }
 
  return 0;
}
