/*
1. MinAbsSumOfTwo
Find the minimal absolute value of a sum of two elements.

Task description

Let A be a non-empty zero-indexed array consisting of N integers.

The abs sum of two for a pair of indices (P, Q) is the absolute value
|A[P] + A[Q]|, for 0 ≤ P ≤ Q < N.

For example, the following array A:

  A[0] =  1
  A[1] =  4
  A[2] = -3

has pairs of indices (0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2). 
The abs sum of two for the pair (0, 0) is A[0] + A[0] = |1 + 1| = 2. 
The abs sum of two for the pair (0, 1) is A[0] + A[1] = |1 + 4| = 5. 
The abs sum of two for the pair (0, 2) is A[0] + A[2] = |1 + (−3)| = 2. 
The abs sum of two for the pair (1, 1) is A[1] + A[1] = |4 + 4| = 8. 
The abs sum of two for the pair (1, 2) is A[1] + A[2] = |4 + (−3)| = 1. 
The abs sum of two for the pair (2, 2) is A[2] + A[2] = |(−3) + (−3)| = 6. 

Write a function:

int solution(vector<int> &A);

that, given a non-empty zero-indexed array A consisting of N integers,
returns the minimal abs sum of two for any pair of indices in this
array.

For example, given the following array A:

  A[0] =  1
  A[1] =  4
  A[2] = -3

the function should return 1, as explained above.

Given array A:

  A[0] = -8
  A[1] =  4
  A[2] =  5
  A[3] =-10
  A[4] =  3

the function should return |(−8) + 5| = 3.

Assume that:

N is an integer within the range [1..100,000]; each element of array A
is an integer within the range [−1,000,000,000..1,000,000,000].

Complexity:

expected worst-case time complexity is O(N*log(N)); expected
worst-case space complexity is O(N), beyond input storage (not
counting the storage required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/

//TODO: Refactor for cheaper handling of simple cases when A contains: 
//      only positives, or only negatives, or a zero entry.


#include<algorithm>
#include<limits>


class closestTo {

private:
unsigned int l, r; 
const vector<int> &A;

public:
closestTo(const vector<int> &A) : A(A) {};
void setRange(unsigned int l, unsigned int r) { this->l = l ; this->r = r;}
int operator() (int v);        
};

// <l,r) -- nonempty
int closestTo::operator() (int v) {

 unsigned int pivot;
 int result = A[l];
  
 while (l < r) {
    
     pivot = (l + r) / 2;
 
     if (abs(result - v) > abs(A[pivot] - v))      
       result = A[pivot];    
     
     if (A[pivot] < v) l = pivot + 1;
     if (A[pivot] > v) r = pivot;
     if (A[pivot] == v) return v;
 }
 
        
 return result;   
    
}



int solution(vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)

int result = numeric_limits<int>::max();
int candidate;


sort(A.begin(), A.end());

unsigned int N = A.size();


closestTo ct(A);

for(int v: A) {

 ct.setRange(0,N);    
 candidate = ct(-v);    
 
 if (abs(v + candidate) < result)
   result = abs(v + candidate);
    
}

 return result;
}


