/*
AbsDistinct

Compute number of distinct absolute values of sorted array elements.
Task description A non-empty zero-indexed array A consisting of N
numbers is given. The array is sorted in non-decreasing order. The
absolute distinct count of this array is the number of distinct
absolute values among the elements of the array.

For example, consider array A such that:

  A[0] = -5
  A[1] = -3
  A[2] = -1
  A[3] =  0
  A[4] =  3
  A[5] =  6

The absolute distinct count of this array is 5, because there are 5
distinct absolute values among the elements of this array, namely 0,
1, 3, 5 and 6.

Write a function:

int solution(vector<int> &A);
that, given a non-empty zero-indexed array A consisting of N numbers,
returns absolute distinct count of array A.

For example, given array A such that:

  A[0] = -5
  A[1] = -3
  A[2] = -1
  A[3] =  0
  A[4] =  3
  A[5] =  6
the function should return 5, as explained above.

Assume that:

N is an integer within the range [1..100,000]; each element of array A
is an integer within the range [−2,147,483,648..2,147,483,647]; array
A is sorted in non-decreasing order.  

Complexity:

expected worst-case time complexity is O(N); expected worst-case space
complexity is O(N), beyond input storage (not counting the storage
required for input arguments).

Elements of input arrays can be modified.  

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/


//Remark: Space complexity is O(1), which is better than expected;


// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;


#include<limits>


bool decreaseRight(int left, int right, const vector<int> &A) {

if (right <= 0)
  return false;

if  ( !( (A[left] < 0) && (A[right] > 0)))
   return false;
   
if (A[left] == numeric_limits<int>::min()) 
   return false;
 
 return (-A[left] < A[right]);
}


int solution(vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)

int N = A.size();
int left = 0, right = N-1, result = 0;


while (left < N) {
 
  if ( (left < N-1) && ( A[left] == A[left+1])) { 
   left++;   
   continue;      
  }
   
  if ( decreaseRight(left, right,A)) {
    right--;
    continue;
    }

  if ( (right > left) && (A[left] + A[right] == 0) ) {
     left++;  
     continue; 
  }

  result++;
  left++;
}

  return result;
}

