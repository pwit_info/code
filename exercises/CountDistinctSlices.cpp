/*
 CountDistinctSlices

Count the number of distinct slices (containing only unique numbers).
Task description An integer M and a non-empty zero-indexed array A
consisting of N non-negative integers are given. All integers in array
A are less than or equal to M.

A pair of integers (P, Q), such that 0 ≤ P ≤ Q < N, is called a slice
of array A. The slice consists of the elements A[P], A[P + 1], ...,
A[Q]. A distinct slice is a slice consisting of only unique
numbers. That is, no individual number occurs more than once in the
slice.

For example, consider integer M = 6 and array A such that:

    A[0] = 3
    A[1] = 4
    A[2] = 5
    A[3] = 5
    A[4] = 2

There are exactly nine distinct slices: (0, 0), (0, 1), (0, 2), (1,
1), (1, 2), (2, 2), (3, 3), (3, 4) and (4, 4).

The goal is to calculate the number of distinct slices.

Write a function:

int solution(int M, vector<int> &A);

that, given an integer M and a non-empty zero-indexed array A
consisting of N integers, returns the number of distinct slices.

If the number of distinct slices is greater than 1,000,000,000, the
function should return 1,000,000,000.

For example, given integer M = 6 and array A such that:

    A[0] = 3
    A[1] = 4
    A[2] = 5
    A[3] = 5
    A[4] = 2
the function should return 9, as explained above.

Assume that:

N is an integer within the range [1..100,000];
M is an integer within the range [0..100,000];
each element of array A is an integer within the range [0..M].
Complexity:

expected worst-case time complexity is O(N); expected worst-case space
complexity is O(M), beyond input storage (not counting the storage
required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/


// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

#include<iostream>
#include<limits>


//Returns number of subslices of slice <left, right> starting in left
int getSliceCount(int left, int right) {

 if (right < left) {
     return 0;
 }

 int size = right - left + 1;
 return size;

}

inline bool insertable(const vector<bool> &cache, int v) {
    return !cache[v];
}

inline void insert(vector<bool> &cache, int v) {
    cache[v] = true;
}

inline void remove(vector<bool> &cache, int v) {
    cache[v] = false;
}



constexpr int MAGIC_NUMBER = 1000000000;

int solution(int M, vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)


//cout << numeric_limits<int>::max();

int N = A.size();
vector<bool> cache(M + 1, false);
int result = 0;


int left =0, right = 0;

while( right < N) {
 
 if (insertable(cache, A[right])) {
     insert(cache, A[right++]);  
 } else {
     result+= getSliceCount(left, right-1); 
     if (result > MAGIC_NUMBER)
       return MAGIC_NUMBER;
     remove(cache, A[left++]);    
 }       
 
}
  
while( left < N) { 
  result+= getSliceCount(left++, right-1);
  if (result > MAGIC_NUMBER)
    return MAGIC_NUMBER;
}
  return result;
}


