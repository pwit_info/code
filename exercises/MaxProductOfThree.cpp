/*
1. MaxProductOfThree
Maximize A[P] * A[Q] * A[R] for any triplet (P, Q, R).


Task description 
A non-empty zero-indexed array A consisting of N
integers is given. The product of triplet (P, Q, R) equates to A[P] *
A[Q] * A[R] (0 ≤ P < Q < R < N).

For example, array A such that:

  A[0] = -3
  A[1] = 1
  A[2] = 2
  A[3] = -2
  A[4] = 5
  A[5] = 6
contains the following example triplets:

(0, 1, 2), product is −3 * 1 * 2 = −6
(1, 2, 4), product is 1 * 2 * 5 = 10
(2, 4, 5), product is 2 * 5 * 6 = 60
Your goal is to find the maximal product of any triplet.

Write a function:

int solution(vector<int> &A); that, given a non-empty zero-indexed
array A, returns the value of the maximal product of any triplet.

For example, given array A such that:

  A[0] = -3
  A[1] = 1
  A[2] = 2
  A[3] = -2
  A[4] = 5
  A[5] = 6
the function should return 60, as the product of triplet (2, 4, 5) is maximal.

Assume that:

N is an integer within the range [3..100,000]; each element of array A
is an integer within the range [−1,000..1,000].  Complexity:

expected worst-case time complexity is O(N*log(N)); expected
worst-case space complexity is O(1), beyond input storage (not
counting the storage required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/

// pwit: Observe that the solution below is better than expected: time
// complexity is O(N). Space complexity remains O(1)

// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;


inline int max(int a, int b) {
  return (a > b) ? a : b;
}

inline int min(int a, int b) {
  return (a < b) ? a : b;   
}

inline int max(int a, int b, int c) {
  return max(a, max(b, c));            
}

// Assumes:    -
// Guarantees: a <= b <= c
inline void sortThreeA(int &a, int &b, int &c) {
 
 int ar, br, cr;
 
 ar = br = cr = 0;
 cr =  max(a, b, c);          

if (cr == a) {
     br = max(b, c);
     ar = min(b, c);
} else
if (cr == b) {
     br = max(a, c);
     ar = min(a, c);
} else
if (cr == c) { 
     br = max(a, b);
     ar = min(a, b);    
 }
    
 a = ar; b = br; c = cr;        
}

// Assumes:    -
// Guarantees: a >= b >= c
inline void sortThreeD(int &a, int &b, int &c) {
a = -a; b = -b; c = -c;
sortThreeA(a, b, c);
a = -a; b = -b; c = -c;
}


// Assumes:    a <= b <= c
// Guarantees: a <= b <= c and sequence a, b, c 
//             contains three greatest values 
//             among a, b, c, d
inline void insertThreeA(int &a, int &b, int &c, int d) {
 
 if (d > c) { 
  a = b;
  b = c;
  c = d;     
 } 
 else 
 if (d > b) {
  a = b;   
  b = d;     
 }
 else
 if (d > a) {
  a = d;   
 }       
}

// Assume:    a >= b >= c
// Guarantee: a >= b >= c and sequence a, b, c
//            contains three smalest values among
//            a, b, c, d

inline void insertThreeD(int &a, int &b, int &c, int d) {
int ar = -a;
int br = -b;
int cr = -c;

insertThreeA(ar, br, cr, -d);

a = -ar;
b = -br;
c = -cr;
}


int solution(vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)
int a = A[0], b = A[1], c = A[2];

sortThreeA(a,b,c);

int N = A.size();
for (int i = 3; i < N; i++) {
    insertThreeA(a, b, c, A[i]);   
}

//cout << a << b << c <<std::endl;

int a_min = A[0], b_min = A[1], c_min = A[2];

sortThreeD(a_min,b_min,c_min);

//cout << a_min << b_min << c_min <<std::endl;

for (int i = 3; i < N; i++) {
    insertThreeD(a_min, b_min, c_min, A[i]);   
}

//cout << a_min << b_min << c_min <<std::endl;

return ( a * b * c > c_min * b_min * c) ? a*b*c : c_min * b_min *c;

}
