/*
1. CountFactors
Count factors of given number n.

Task description

A positive integer D is a factor of a positive integer N if there
exists an integer M such that N = D * M.

For example, 6 is a factor of 24, because M = 4 satisfies the above
condition (24 = 6 * 4).

Write a function:

int solution(int N);
that, given a positive integer N, returns the number of its factors.

For example, given N = 24, the function should return 8, because 24
has 8 factors, namely 1, 2, 3, 4, 6, 8, 12, 24. There are no other
factors of 24.

Assume that:

N is an integer within the range [1..2,147,483,647].

Complexity:

expected worst-case time complexity is O(sqrt(N)); expected worst-case
space complexity is O(1).

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/


// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

#include<limits>

int solution(int N) {
    // write your code in C++11 (g++ 4.8.2)

int factors = 0;


unsigned int i;

for(i = 1; i*i < static_cast<unsigned int>(N); i++) {
     
     factors+= static_cast<int>((N % i) == 0) << 1;
    
}

//TODO: check if the usage of "i" after loop conforms c++14 standard

if (i*i == static_cast<unsigned int>(N)) factors ++;


  return factors;
}


