/*
1. NumberOfDiscIntersections
Compute the number of intersections in a sequence of discs.

Task description 

We draw N discs on a plane. The discs are numbered from 0 to N − 1. A
zero-indexed array A of N non-negative integers, specifying the
radiuses of the discs, is given. The J-th disc is drawn with its
center at (J, 0) and radius A[J].

We say that the J-th disc and K-th disc intersect if J ≠ K and the
J-th and K-th discs have at least one common point (assuming that the
discs contain their borders).

The figure below shows discs drawn for N = 6 and A as follows:

  A[0] = 1
  A[1] = 5
  A[2] = 2
  A[3] = 1
  A[4] = 4
  A[5] = 0


There are eleven (unordered) pairs of discs that intersect, namely:

discs 1 and 4 intersect, and both intersect with all the other discs;
disc 2 also intersects with discs 0 and 3.  

Write a function:

int solution(vector<int> &A); that, given an array A describing N
discs as explained above, returns the number of (unordered) pairs of
intersecting discs. The function should return −1 if the number of
intersecting pairs exceeds 10,000,000.

Given array A shown above, the function should return 11, as explained above.

Assume that:

N is an integer within the range [0..100,000]; each element of array A
is an integer within the range [0..2,147,483,647].  

Complexity:

expected worst-case time complexity is O(N*log(N)); expected
worst-case space complexity is O(N), beyond input storage (not
counting the storage required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/





#include<vector>
#include<algorithm>
#include<iostream>

using namespace std;

typedef struct {
  int left;
  int right;
} Interval;

// ostream& operator<<(ostream &os, Interval il) {

//   os << "(" << il.left << "," << il.right << ")";
//   return os;
// }

// ostream& operator<<(ostream &os, vector<Interval> iv) {

//   bool first = true;
  
//   for (auto &v : iv) {
//     if (!first) os << ",";
//     else first = false;
    
//     os << v;
//   }
  
//   return os;
// }



class IntervalComp {
public:
  bool operator() (Interval a, Interval b) {
    return a.left < b.left; 
  }

  bool operator() (int a, Interval b) {
    return a < b.left;
  }
};

// Assumes:    left <= right
// Guarantees: cuts c to interval [left,right]
inline int cut(int c, int left, int right) {

  if (c < left) return left;
  if (c > right) return right;
  return c;
}


int solution(vector<int> &A) {

  int N = A.size();

  // normalize A, i.e. cut all radiuses to N
  for(auto &v : A) {
    if (v > N) v = N;
  }
  

  vector<Interval> iv(N, {0,0});

  vector<Interval>::iterator it = iv.begin();
  
  for (int i = 0; i < N; i++) {
    *it = {cut(i - A[i],0,N-1), cut(i + A[i],0,N-1)};
    it++;
  }


  //  cout << iv << endl;
  
  IntervalComp ic;
  sort(iv.begin(), iv.end(), ic);

  //  cout << iv << endl;

  
  int result = 0;

   for(auto it = iv.begin(); it != iv.end(); it++) {

     auto ir = upper_bound(it, iv.end(), (*it).right, ic);
     if (result  - 1 > 10000000 - (ir - it))
       return -1;
     else
     result +=  ir - it - 1;  
    
   }
  return result;
}
  


int main(void) {

  vector<int> A = {1,5,2,1,4,0};

  cout << solution(A);

  return 0;
}

