/*
1. PermMissingElem
Find the missing element in a given permutation.

A zero-indexed array A consisting of N different integers is
given. The array contains integers in the range [1..(N + 1)], which
means that exactly one element is missing.

Your goal is to find that missing element.

Write a function:

int solution(vector<int> &A); that, given a zero-indexed array A,
returns the value of the missing element.

For example, given array A such that:

  A[0] = 2
  A[1] = 3
  A[2] = 1
  A[3] = 5
the function should return 4, as it is the missing element.

Assume that:

N is an integer within the range [0..100,000]; the elements of A are
all distinct; each element of array A is an integer within the range
[1..(N + 1)].  Complexity:

expected worst-case time complexity is O(N); expected worst-case space
complexity is O(1), beyond input storage (not counting the storage
required for input arguments).  Elements of input arrays can be
modified.  

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/

// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

int solution(vector<int> &A) {
    // write your code in C++11 (g++ 4.8.2)

 int sweeper = 0;
 int N = A.size();
 int current, next;
 
 //mark
 while (sweeper < N) {
     
    // A[sweeper] != 0
     
    current = A[sweeper] - 1;
        
     while( (current != N) && (A[current] != 0)) {
      
      next = A[current] - 1;
      A[current] = 0;
      current = next;     
     }
        
    do {  
      sweeper++;     
    } while ((sweeper < N) && ( A[sweeper] == 0) ); 
    
 }

//find

  sweeper = 0;
  while ( (sweeper < N) && (A[sweeper] == 0) )
    sweeper++;
    
  return sweeper + 1;    
}
