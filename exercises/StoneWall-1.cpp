/*
StoneWall
Cover "Manhattan skyline" using the minimum number of rectangles.

Solution to this task can be found at our blog.

You are going to build a stone wall. The wall should be straight and N
meters long, and its thickness should be constant; however, it should
have different heights in different places. The height of the wall is
specified by a zero-indexed array H of N positive integers. H[I] is
the height of the wall from I to I+1 meters to the right of its left
end. In particular, H[0] is the height of the wall's left end and
H[N−1] is the height of the wall's right end.

The wall should be built of cuboid stone blocks (that is, all sides of
such blocks are rectangular). Your task is to compute the minimum
number of blocks needed to build the wall.

Write a function:

int solution(vector<int> &H);

that, given a zero-indexed array H of N positive integers specifying
the height of the wall, returns the minimum number of blocks needed to
build it.

For example, given array H containing N = 9 integers:

  H[0] = 8    H[1] = 8    H[2] = 5
  H[3] = 7    H[4] = 9    H[5] = 8
  H[6] = 7    H[7] = 4    H[8] = 8

the function should return 7. The figure shows one possible
arrangement of seven blocks.



Assume that:

N is an integer within the range [1..100,000]; each element of array H
is an integer within the range [1..1,000,000,000].  Complexity:

expected worst-case time complexity is O(N); expected worst-case space
complexity is O(N), beyond input storage (not counting the storage
required for input arguments).

Elements of input arrays can be modified.

Copyright 2009–2016 by Codility Limited. All Rights
Reserved. Unauthorized copying, publication or disclosure prohibited.
*/


//pwit: Warning, quality of this snippet is poor. Scores only 35% on codility!



// you can use includes, for example:
// #include <algorithm>

// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

//#include<deque>
#include<vector>
#include "roundbuffer.hpp"



using namespace std;

const int start_marker = -1;

int solution(vector<int> &H) {
    // write your code in C++11 (g++ 4.8.2)

int result = 0;
// deque<int> dq(H.begin(), H.end());
 queue<int, RoundBuffer<int>> dq(H.size()+1);

 for(auto it = H.begin(); it != H.end(); it++) {

   dq.push(*it);

 }
 
 int max;
 int crr;
 dq.push(start_marker);
  
 result = 0;
 max = dq.front();
 
 while (dq.size() > 1) {
     
    crr = dq.front();
    dq.pop();

    if (crr == start_marker) {
        
        max = dq.front();
        if (dq.back() != start_marker) dq.push(start_marker);
        result++;
    } else {
        
        if (crr > max) {
         
         dq.push(crr - max);   
            
        } else
        if (crr < max) {
            
         result++;
         max = crr;
         if ( dq.back() != start_marker) dq.push(start_marker);
        }
        
        
    }
          
 }

  result++;

  return result;

}


int main(void) {

  vector<int> H = {8, 8, 5, 7, 9, 8, 7, 4, 8};

  cout << solution(H) << endl;

  return 0;
}
