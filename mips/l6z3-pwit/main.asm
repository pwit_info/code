#Napisz program, który zlicza ile jedynek znajduje się w bitowej reprezentacji rejestru $s0.

.data 

.globl main
.text

main:


#count_ones tests

addi $a0, $zero, 0
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 1
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 2
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 3
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 4
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 5
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 6
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 7
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 128
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 255
la $a1, count_ones
jal count_and_print

addi $a0, $zero, 0xFFFFFFFF
la $a1, count_ones
jal count_and_print

#count_ones_fast tests

addi $a0, $zero, 0
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 1
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 2
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 3
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 4
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 5
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 6
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 7
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 128
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 255
la $a1, count_ones_fast
jal count_and_print

addi $a0, $zero, 0xFFFFFFFF
la $a1, count_ones_fast
jal count_and_print





#That's all, terminate program.
add $v0, $zero, 10
syscall

################################################################################
#Assumes:     $a0 - an integer, $a1 - address of a function that counts
#             the number of ones in $a0 and returns it in $v0
#Guarantees:  prints the number of 1s in $a0 
#
################################################################################

.data
 msg_part1: .asciiz "The number of 1s in "
 msg_part2: .asciiz " is: "
.text
count_and_print:

addi $sp, $sp, -16          #
sw $ra, 0($sp)              # prepare stack frame
sw $a0, 4($sp)              #
sw $a1, 8($sp)              #

la $a0, msg_part1
addi $v0, $zero, 4
syscall

lw $a0, 4($sp)
addi $v0, $zero, 1
syscall

la $a0, msg_part2
addi $v0, $zero, 4
syscall

lw $a0, 4($sp)              #
lw $a1, 8($sp)              # call counting function
jalr $a1                    #

move $a0, $v0               
la $v0, 1          
syscall

addi $a0, $zero, '\n'
addi $v0, $zero, 11
syscall

lw $ra, 0($sp)              # restore return address,         
addi $sp, $sp, 16           # remove stack frame
jr $ra                      # and return




################################################################################
#Assumes:     -
#Guarantees:  $v0 contains the number of 1s in binary repr. of $a0
#
################################################################################
count_ones:

.eqv tmp, $t0
.eqv result $v0
.eqv arg $a0

xor result, result, result

counting_loop:
andi tmp, arg, 1 
add result, result, tmp
srl arg, arg, 1
bnez  arg, counting_loop
jr $ra



################################################################################
#Assumes:     -
#Guarantees:  $v0 contains the number of 1s in binary repr. of $a0
#
#Improved version
#
#Comment: Is it really faster than count_ones?  Unwinding a loop in count_ones
#        gives 32*3 instructions. Here we have 6*6 instructions. 
#        So its faster, roughly.
################################################################################
count_ones_fast:

.eqv arg $a0
.eqv mask $t0
.eqv result $v0
.eqv tmp1 $t1
.eqv tmp2 $t2

xor result, result, result


#load 01010101010101010101010101010101b = 0x55555555 to mask
lui mask, 0x5555
ori mask, 0x5555
#perform 1st step
and tmp1, arg, mask 
srl tmp2, arg, 1
and tmp2, tmp2, mask
addu arg, tmp1, tmp2

#load 00110011001100110011001100110011b = 0x33333333 to mask
lui mask, 0x3333
ori mask, 0x3333
#perform 2nd step
and tmp1, arg, mask 
srl tmp2, arg, 2
and tmp2, tmp2, mask
addu arg, tmp1, tmp2

#load 00001111000011110000111100001111b = 0x0F0F0F0F to mask
lui mask, 0x0F0F
ori mask, 0x0F0F
#perform 4rd step
and tmp1, arg, mask 
srl tmp2, arg, 4
and tmp2, tmp2, mask
addu arg, tmp1, tmp2

#load 00000000111111110000000011111111b = 0x00FF00FF to mask
lui mask, 0x00FF
ori mask, 0x00FF
#perform 5th step
and tmp1, arg, mask 
srl tmp2, arg, 8
and tmp2, tmp2, mask
addu arg, tmp1, tmp2

#load 00000000000000001111111111111111b = 0x0000FFFF to mask
lui mask, 0x0000
ori mask, 0xFFFF
#perform 6th step
and tmp1, arg, mask 
srl tmp2, arg, 16
and tmp2, tmp2, mask
addu arg, tmp1, tmp2

move result, arg

jr $ra

###############################################################################
