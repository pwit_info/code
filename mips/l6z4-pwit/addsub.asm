.globl perform_task

.text

################################################################################
# Assumes:    $a0 --- contains two integers encoded as specified above
# Guarantees: upon termination $v0 contains their sum and difference,
#             with the same encoding. 
#
# Over/under-flows are ignored. 
################################################################################
perform_task:

.eqv arg       $a0
.eqv result    $v0
.eqv a_number  $t0
.eqv b_number  $t1
.eqv tmp       $t2
.eqv sum       $t3
.eqv diff      $t4

#extract a
sra a_number, arg, 16
#extract b
sll b_number, arg, 16
sra b_number, b_number, 16
#perform arith ops
add sum, a_number, b_number
sub diff, a_number, b_number
#clear upper halfwords of sum and diff
andi sum, sum, 0xFFFF
andi diff, diff, 0xFFFF
#pack result
sll result, sum, 16
or result, result, diff
#return 
jr $ra
