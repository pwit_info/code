#Załóżmy, że rejestr $a0 przechowuje dwie liczby szesnastobitowe:
# liczbę a na swoich bardziej znaczących bitach oraz liczbę b na
# swoich mniej znaczących bitach. Obie zakodowane w reprezentacji
# uzupełnień do 2.  W asemblerze MIPS napisz program, który
# umieści w rejestrze $v0 dwie liczby szesnastobitowe: na bardziej
# znaczących bitach liczbę a + b, a na mniej znacz¡cych --- liczbę
# a − b.  Zakładamy, że błędy przepełnienia nie wystąpią (nie
# musisz się nimi przejmować).

.data

# Each data entry is a pair of halfwords representing  numbers a and b.
# Warning: MARS is little endian
data_examples:    .half 1, 2, 2, 2, 2, 1
expected_results: .half 1, 3, 0, 4, -1, 3
num_of_entries:   .word 3

.text
main:

la $a0, data_examples
la $a1, expected_results
la $a2, num_of_entries
jal perform_tests


#That's all, terminate program.
add $v0, $zero, 10
syscall


################################################################################
#Assumes:      $a0 - address of table with example arguments to perform_task
#              $a1 - address of table with expected results
#              $a2 - number of entries in both tables
#
#Guarantees:   Checks if expected results match return values of perform_task
#              run on example arguments. Technically,
#              $v0 is an index of the first entry on which perform_task fails. 
#              If perform_task is correct on all entries then $v0 is $a2.  
################################################################################


perform_tests:

.eqv examples       $a0
.eqv results        $a1 
.eqv num_of_entries $a2
.eqv crr_entry $t0

addi $sp, $sp, -24
sw $ra, 0($sp)
sw examples, 4($sp)
sw results, 8($sp)
sw num_of_entries, 12($sp)


.eqv tmp $t1
.eqv crr_entry_addr $t2

add crr_entry, $zero, $zero

start_loop:
slt tmp, crr_entry, num_of_entries
beq tmp, $zero, end_loop

sll tmp, crr_entry, 2
add crr_entry_addr, examples, tmp

sw crr_entry, 16($sp)
lw $a0, 0(crr_entry_addr)
jal perform_task
lw crr_entry, 16($sp)
lw examples, 4($sp)
lw results, 8($sp)
lw num_of_entries, 12($sp)


sll tmp, crr_entry, 2
add crr_entry_addr, results, tmp
lw tmp, 0(crr_entry_addr)
bne tmp, $v0, end_loop

addi crr_entry, crr_entry, 1
j start_loop
end_loop:

lw $ra, 0($sp)
add $sp, $sp, 24
move $v0, crr_entry
jr $ra

