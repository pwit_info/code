.globl qsort
.text

###############################################################################
# Assumes:      $a0 --- address of a non-empty integer array
#               $a1, $a2 --- indices within array, $a1 <= #a2
# Guarantees:   $a0[$a1...$a2] is sorted in non-descending order
# Invokes:      qsort_rec
# Invoked by:   global
###############################################################################
qsort:
#create stack frame
add $sp, $sp, -8
sw $ra, 0($sp)

#prepare arguments for qsort_rec
sll $a1, $a1, 2
add $a1, $a1, $a0
sll $a2, $a2, 2
add $a2, $a2, $a0
move $a0, $a1
move $a1, $a2
jal qsort_rec

#restore $ra and erase stack frame
lw $ra, 0($sp)
add $sp, $sp, 8
jr $ra
#qsort ends here
###############################################################################




###############################################################################
# Assumes:     $a0, $a1 --- addresses of left and righ pointers  
#              in a word array, $a1 <= #a2
# Guarantees:  part of table defined $a0...$a1 is sorted 
#              in non-descending order
# Invokes:     -
# Invoked by:  qsort
# TODO: optimize register usage
###############################################################################
qsort_rec: #qsort_rec(left, right)

#variable declaration
.eqv left $a0
.eqv right $a1

.eqv tmp $t0
.eqv pivot_value $t1
.eqv lftAddr $t2                       # left pointer, initially lftAddr=left
.eqv rgtAddr $t3                       # right pointer, initially rgtAddr=right
.eqv lftValue $t4                      # value at lftAddr
.eqv rgtValue $t5                      # value at rgtAddr

.eqv sweeper $t6


blt left, right, at_least_two_elements_array    # if !(left < right) {
jr $ra                                          # return;
at_least_two_elements_array:                    # }



lw pivot_value, 0(left)                         # initialize pivot_value, 
move lftAddr, left                              # lftAddr, 
move rgtAddr, right                             # and rgtAddr


                                                
lw tmp, 0(rgtAddr)                              # switch elements at 
sw tmp, 0(lftAddr)                              # lftAddr and
sw pivot_value, 0(rgtAddr)                      # rgtAddr

addi rgtAddr, rgtAddr, -4                       # 

move sweeper, lftAddr


loop_start:                                     #do { ....
lw lftValue, 0(lftAddr)                         #
ble pivot_value, lftValue, skip_swap            # if (lftValue < pivot_value) {
lw tmp, 0(sweeper)                              #
sw lftValue, 0(sweeper)                         # swap &sweeper with &lftAddr
sw tmp, 0(lftAddr)                              #
addi, sweeper, sweeper,  4                      # increase sweeper
skip_swap:                                      # }
#

addi lftAddr, lftAddr, 4

ble lftAddr, rgtAddr, loop_start
#blt rgtAddr, lftAddr, loop_start                # } while (lftAddr <= rgtAddr);

     
                              
                                             
addi rgtAddr, rgtAddr, 4                       #   
lw tmp, 0(sweeper)                             # switch pivot with an element
sw tmp, 0(rgtAddr)                             # at sweeper
sw pivot_value, 0(sweeper)                     #

                                                                          
addi $sp, $sp, -16                             #stack must be 8bytes alligned
sw $ra, 0($sp)
sw lftAddr, 4($sp)
sw sweeper, 8($sp)
sw rgtAddr, 12($sp)


move $a0, left
addi tmp, sweeper, -4
move $a1, tmp
jal qsort_rec                                 # qsort tableAddr left sweeper-4



lw lftAddr, 4($sp)
lw sweeper, 8($sp)
lw rgtAddr, 12($sp)

addi tmp, sweeper, 4
move $a0, tmp
move $a1, rgtAddr
jal qsort_rec                                # qsort taleAddr sweeper+4 right

lw $ra, 0($sp)
add $sp, $sp, 16

jr $ra 
#function qsort ends here
###############################################################################

