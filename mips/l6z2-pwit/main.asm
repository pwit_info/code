# Napisz program, który sortuje tablicę liczb całkowitych metodą quicksort
  
.data 
table1: .word 1
size1:  .word 1
table2: .word 1, 2
size2:  .word 2
table3: .word 2, 1
size3:  .word 2
table4: .word 1, 2, 3
size4:  .word 3
table5: .word 1, 3, 2
size5:  .word 3
table6: .word 2, 1, 3
size6:  .word 3
table7: .word 2, 3, 1
size7:  .word 3
table8: .word 3, 1, 2
size8:  .word 3
table9: .word 3, 2, 1
size9:  .word 3
table10: .word 7, 3, 2, 11, 6, 7
size10:  .word 6



.globl main
.text


main:

la $a0, table1
lw $a1, size1
jal sort_and_print

la $a0, table2
lw $a1, size2
jal sort_and_print

la $a0, table3
lw $a1, size3
jal sort_and_print

la $a0, table4
lw $a1, size4
jal sort_and_print

la $a0, table5
lw $a1, size5
jal sort_and_print

la $a0, table6
lw $a1, size6
jal sort_and_print

la $a0, table7
lw $a1, size7
jal sort_and_print

la $a0, table8
lw $a1, size8
jal sort_and_print

la $a0, table9
lw $a1, size9
jal sort_and_print

la $a0, table10
lw $a1, size10
jal sort_and_print


#That's all, terminate program.
add $v0, $zero, 10
syscall



###############################################################################
#                      sort_and_print (table, table_size)
#
# Assumes: table($a0) is an array of table_size($a1) integers
# Guarantees: table is sorted and printed to stdout
# Invokes: qsort
# Invoked by: main
# TODO: optimize register usage, correct usage of saved register $s0!!!
###############################################################################
sort_and_print:

.eqv table $a0
.eqv table_size $a1
#print array
.eqv cnt $s0
.eqv tmp $t1
.eqv tmp1 $t2



addi $sp, $sp, -16                     #
sw $ra, 0($sp)                         # create and fill a stack frame 
sw table, 4($sp)                       #
sw table_size, 8($sp)                  #


addi table_size, table_size, -1        # prepare arguments for qsort
move $a2, table_size                   # and run it; $a0 is already set
xor $a1, $a1, $a1                      #
jal qsort                              #





xor cnt, cnt, cnt

print:

lw $ra, 0($sp)                         # restore data from stack frame                 
lw table, 4($sp)                       # (needed because previous invocation
lw table_size, 8($sp)                  #  of qsort or syscalls)
     

slt tmp, cnt, table_size
beqz tmp, finish

move tmp, table
sll tmp1, cnt, 2
add tmp, tmp, tmp1
lw $a0, 0(tmp)
addi $v0, $zero, 1
syscall

li $a0, ' '
addi $v0, $zero, 11
syscall

addi cnt, cnt, 1
j print
finish:

li $a0, '\n'                           #
addi $v0, $zero, 11                    # print newline
syscall                                #

addi $sp, $sp, 16                      #remove stack frame
jr $ra
###############################################################################
