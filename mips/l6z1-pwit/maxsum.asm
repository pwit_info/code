.globl maxsum
.text

###############################################################################
# Assume: $a0 --- address of a non-empty integer array
#         $a1 --- size of the aray
# Guarantee: $v0 --- sum of the array, $v1 --- max value in the array
###############################################################################
maxsum: 

.eqv tableAddr $a0
.eqv tableSize $a1

#Sum and Max are return registers
.eqv Sum $v0
.eqv Max $v1
#Cnt is a loop counter
.eqv Cnt $t0
#valueAtCnt stores $a0[Cnt]
.eqv valueAtCnt $t4
#tmp is used as a temporal variable, usually within 2-5 lines
.eqv tmp $t1

#initialize Sum and Max
move Sum, $zero
lw Max, 0(tableAddr) # as assumed table at $a0 is non-empty

#initialize Cnt
xor Cnt,Cnt,Cnt #Cnt = 0

loop_start:
sll tmp, Cnt, 2  #Cnt *= 4
add tmp,tableAddr,tmp
lw valueAtCnt, 0(tmp) # $valueAtCnt - array's value at index Cnt 

#update sum
add Sum, Sum, valueAtCnt
#update max
slt tmp, Max, valueAtCnt  # $tmp = Max < valueAtCnt
beqz tmp, skip_max_update
move Max, valueAtCnt
skip_max_update:
#update loop counter
addi Cnt,Cnt,1
#loop condition
bne Cnt,tableSize,loop_start
#loop ends here
jr $ra 
#function maxsum ends here
###############################################################################

