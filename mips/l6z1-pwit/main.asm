# Napisz program, który wylicza sum¦ oraz maksimum z tablicy liczb caakowitych, której adres zadany
# jest w rejestrze $a0, a rozmiar w rejestrze $a1. Wynik umie±¢ w rejestrze $v0 i $v1.
#Dodatkowe założenie: tablica jest niepusta!

.data 
sum_string: .asciiz "Sum of the array is:"
max_string: .asciiz "\nMax element of the array is:"
table: .word 1,2,3,42,5,11,3
size:  .word 7
.globl main
.text

main:

#run maxsum
la $a0, table
la $a1, size
lw $a1, 0($a1)
jal maxsum

#store results of maxsum in s-registers
move $s0, $v0
move $s1, $v1

#print sum of the array
addi $v0, $zero, 4
la $a0, sum_string
syscall 
addi $v0, $zero, 1
move $a0, $s0
syscall

#print max element of the array
addi $v0, $zero, 4
la $a0, max_string
syscall
addi $v0, $zero, 1
move $a0, $s1
syscall

#That's all, terminate program.
add $v0, $zero, 10
syscall
