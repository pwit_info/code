/*******************************************************************************
 * Tests for Point, Vector and StraightLine classes.
 ******************************************************************************/

#include "zad2prosta.hpp"
#include "gtest/gtest.h"

#include<cmath>

/* Point class tests **********************************************************/

TEST(PointConstruction, SimpleTests) {

	Point p;

	EXPECT_EQ(p.getX(), 0);
	EXPECT_EQ(p.getY(),0);

	Point p1(1.1, 2.2);
	EXPECT_EQ(p1.getX(), 1.1);
	EXPECT_EQ(p1.getY(),2.2);
}

TEST(PointConstruction, TranslationOfaPointbyaVector) {

	Point p(1.1,1.1);
	Vector v(-1.1, 1.2);

	Point result(p, v);
	EXPECT_EQ(result.getX(), 0);
	EXPECT_EQ(result.getY(), 2.3);

}

/* End of Point class tests ***************************************************/



/* Vector class tests *********************************************************/


TEST(VectorConstruction, SimpleTests) {

	Vector v;

	EXPECT_EQ(v.getDx(), 0);
	EXPECT_EQ(v.getDy(),0);

	Vector v1(1.1, 2.2);
	EXPECT_EQ(v1.getDx(), 1.1);
	EXPECT_EQ(v1.getDy(),2.2);

}
/* End of Vector class tests **************************************************/


/* StraightLine class tests ***************************************************/

TEST(StraightLineConstruction, SimpleTests) {

  StraightLine sl(1,2.2,3);

  EXPECT_EQ(sl.getA(), 1);
  EXPECT_EQ(sl.getB(), 2.2);
  EXPECT_EQ(sl.getC(), 3);


  EXPECT_THROW(StraightLine sl1(0,0,5), std::invalid_argument);
  EXPECT_NO_THROW(StraightLine sl1(1,0,5));
  EXPECT_NO_THROW(StraightLine sl1(0,1,5));
  EXPECT_NO_THROW(StraightLine sl1(0,1,0));
  EXPECT_NO_THROW(StraightLine sl1(1,0,0));


}


TEST(StraightLinesEqual, SimpleTests) {

	struct {
		StraightLine src, tgt;
	}  test_table[] {

			{{1,2,3}, {1,2,3}}, {{1,2,3},{2,4,6}}, {{1,2,3}, {1*1.3, 2*1.3, 3*1.3}},
			{{1,2,3}, {1*M_PI, 2*M_PI, 3*M_PI}},
			{{1.43,2.234,3.5678}, {1.43*M_PI, 2.234*M_PI, 3.5678*M_PI}}

	};


	for(auto &v: test_table) {
		EXPECT_EQ(v.src, v.tgt);
	}

}


TEST(StraightLineConstruction, FromTwoPoints) {

	Point p(1,1);
	EXPECT_THROW(StraightLine(p,p), std::invalid_argument);

	struct {
		Point p1, p2;
		StraightLine sl;
	       } test_table[] = {

	    		   //{{0,0},{0,0}, {,,}},
	    		   {{0,0},{0,1}, {1,0,0}},
				   {{0,0},{1,0}, {0,1,0}},
				   {{0,0},{1,1}, {1,-1,0}},
				   {{0,1},{0,0}, {1,0,0}},
//				   //{{0,1},{0,1}, {,,}},
				   {{0,1},{1,0}, {1,1,-1}},
				   {{0,1},{1,1}, {0,1,-1}},
				   {{1,0},{0,0}, {0,1,0}},
				   {{1,0},{0,1}, {1,1,-1}},
//				   //{{1,0},{1,0}, {,,}},
				   {{1,0},{1,1}, {1,0,-1}},
				   {{1,1},{0,0}, {1,-1,0}},
				   {{1,1},{0,1}, {0,1,-1}},
				   {{1,1},{1,0}, {1,0,-1}}//,
//				   //{{1,1},{1,1}, {,,}},


		};


	 for( auto &v: test_table) {

		 StraightLine sl(v.p1, v.p2);
		 EXPECT_EQ(sl, v.sl);
	 }

}


TEST(StraightLineConstruction, FromVector) {

	Vector v(1,0);
    StraightLine sl(v);
    StraightLine sl_test(1,0,0);
    StraightLine sl_test1(1,1,0);


    EXPECT_EQ(sl, sl_test);
    EXPECT_NE(sl, sl_test1);


}



TEST(StraightLine, isOrthogonalTest) {

	struct {
		StraightLine sl;
		Vector v;
	       } test_table_positives[] = {
				   {{0,1,0}, {0,1}},
				   {{0,1,1}, {0,10}},
				   {{1,0,0}, {55,0}},
				   {{1,0,1}, {10,0}},
				   {{1,1,0}, {10,10}},
				   {{1,1,1}, {1,1}}
		};


	 for( auto &ent: test_table_positives) {

		 EXPECT_TRUE(ent.sl.isOrthogonal(ent.v));
		 StraightLine false_line = {ent.sl.getA() + 3,
				 	 	 	 	 	ent.sl.getB() + 1,
									ent.sl.getC()};
		 EXPECT_FALSE(false_line.isOrthogonal(ent.v));
	 }





}

TEST(StraightLine, isParallelTest) {
	struct {
			StraightLine sl;
			Vector v;
		       } test_table_positives[] = {
					   {{0,1,0}, {-1,0}},
					   {{0,1,1}, {-10,0}},
					   {{1,0,0}, {0,1}},
					   {{1,0,1}, {0, 10}},
					   {{1,1,0}, {-10,10}},
					   {{1,1,1}, {-1,1}}
			},

			test_table_negatives[] = {
							   {{0,1,0}, {0,1}},
							   {{0,1,1}, {0,10}},
							   {{1,0,0}, {55,0}},
							   {{1,0,1}, {10,0}},
							   {{1,1,0}, {10,10}},
							   {{1,1,1}, {1,1}}
					};


		 for( auto &ent: test_table_positives) {

			 EXPECT_TRUE(ent.sl.isParallel(ent.v));
		 }
		 for( auto &ent: test_table_negatives) {

			 EXPECT_FALSE(ent.sl.isParallel(ent.v));
		 }
}

TEST(StraightLine, containsTest) {

	StraightLine sl1 {1,1,0};

	EXPECT_TRUE(sl1.contains({0,0}));
	EXPECT_TRUE(sl1.contains({1,-1}));
	EXPECT_TRUE(sl1.contains({-1,1}));

	EXPECT_FALSE(sl1.contains({-1,-1}));


}






/* End of StraightLine class tests ********************************************/



