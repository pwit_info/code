/*******************************************************************************
 * Declarations of Vector, Point and StraightLine classes.
 * All these classes represent objects in 2-dimensional space.
 * Implemented methods allow construction and manipulation of these objects.
 ******************************************************************************/

#ifndef ZAD2PROSTA_HPP_
#define ZAD2PROSTA_HPP_

#include<stdexcept>



/*******************************************************************************
 * A Free vector given by directions dx and dy.
 *
 ******************************************************************************/
class Vector {

private:
	double dx, dy; //direction of a vector

public:
	Vector(): dx(0.0), dy(0.0) {}
	Vector(double dx, double dy): dx(dx), dy(dy) {}

	double getDx() const {
		return dx;
	}

	double getDy() const {
		return dy;
	}


	/***************************************************************************
	 *Assumes:    -
     *Guarantees: returns current vector ("this") scaled by a constant s
	 **************************************************************************/
	Vector scale(double s) const;

	/**************************************8************************************
	 * Assumes:    -
	 * Guarantees: returns true iff v is a scaled version of this
	 **************************************************************************/
	bool isScaled(const Vector &v) const;

};

/*******************************************************************************
 *
 * A simple point given by coordinates x, y.
 *
 ********************************************************************************/
class Point {

private:
	double x, y; // coordinates of a point

public:
	Point (): x(0.0), y(0.0) {}
	Point (double x, double y): x(x), y(y) {}
	Point (const Point &p): x(p.x), y(p.y) {}

	/***************************************************************************
	 * Assumes:    -
	 * Guarantees: new point is constructed: translation p by v
	 **************************************************************************/
	Point (const Point &p, const Vector &v);


	double getX() const {
		return x;
	}

	double getY() const {
		return y;
	}

	bool operator==(const Point &p) const {

		return (x == p.x) && (y == p.y);
	}


	bool operator==(int zero) const {

		if (zero)
			throw std::invalid_argument("Points can only be compared with zero.");
		return (!x) && (!y);
	}

};


/*******************************************************************************
 *
 * A straight line given by line equation ax + by + c = 0
 *
 ********************************************************************************/
class StraightLine {

private:
	double a,b,c; // line coefficients

public:
	StraightLine( const StraightLine &l): a(l.a), b(l.b), c(l.c) {};

	/***************************************************************************
	 * Assumes:     -
	 * Guarantees:  direct construction of a straight line
	 * Throws: invalid_argument if both a and b are 0
	 **************************************************************************/
	StraightLine( double a, double b, double c);

	/***************************************************************************
     * Assumes:    -
	 * Guarantees: constructs a straight line from two points
	 * Throws: invalid_argument if p1 == p2
	 **************************************************************************/
	StraightLine( Point p1, Point p2);

	/***************************************************************************
	 * Assumes: -
	 * Guarantees: "prosta ma przechodzić przez
	 * punkt oddalony od początku układu współrzędnych przez zadany wektor
	 * i być prostopadła do tego wektora"
	 */
	StraightLine( const Vector &v);

	/***************************************************************************
	 * Assumes: -
	 * Guarantees: A newly constructed straight line is the line l translated
	 *             by vector v
	 **************************************************************************/
	StraightLine( const StraightLine &l, const Vector &v);



	double getA() const {
		return a;
	}

	double getB() const {
		return b;
	}

	double getC() const {
		return c;
	}

	bool operator==(const StraightLine&) const;
	bool operator!=(const StraightLine &s) const {return !(*this == s );}

	/***************************************************************************
	 * Assumes:    -
	 * Guarantees: - checks if v and this are orthogonal
	 **************************************************************************/
	bool isOrthogonal(const Vector &v) const;

	/***************************************************************************
	 * Assumes:    -
	 * Guarantees: - checks if v and this are parallel
	 **************************************************************************/
	bool isParallel( const Vector &v) const;

	/***************************************************************************
	 * Assumes:    -
	 * Guarantees: - checks if straight line contains a point p
	 **************************************************************************/
	bool contains( const Point &p) const;


private:
	/***************************************************************************
	 * Assumes:    -
	 * Guarantees: -
	 *
	 * Throws: invalid_argument if both a and b are 0.
	 **************************************************************************/
	inline void confirmCorrectCoords(double a, double b) const;

};


#endif /* ZAD2PROSTA_HPP_ */
