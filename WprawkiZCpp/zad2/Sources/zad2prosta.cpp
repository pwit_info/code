/*******************************************************************************
 * Defines methods from Vector, Point and StraightLine classes.
 *
 ******************************************************************************/


#include "zad2prosta.hpp"



/*******************************************************************************
 *
 *Assumes:    -
 *Guarantees: returns current vector ("this") scaled by a constant s
 *
 *
 *TODO: implement move semantics?
 ******************************************************************************/

Vector Vector::scale(double s) const {

	return Vector(s * dx,  s * dy);

}

/**************************************8****************************************
 *
 * Assumes:    -
 * Guarantees: returns true iff v is a scaled version of this
 *
 ******************************************************************************/

bool Vector::isScaled(const Vector &v) const {

 return  ( (dx == 0) && (v.getDx() == 0)) ||
		 ( (dy == 0) && (v.getDy() == 0)) ||
		 ( (dx * v.getDy())  == (dy * v.getDx()));


}

/*******************************************************************************
 * Assumes:    -
 * Guarantees: new point is constructed: translation p by v
 ******************************************************************************/

Point::Point (const Point &p, const Vector &v) {

 x = v.getDx() + p.getX();
 y = v.getDy() + p.getY();
}


/*******************************************************************************
 *
 * Assumes:     -
 * Guarantees:  direct construction of a straight line
 *
 * Throws: invalid_argument if both a and b are 0
 *
 ******************************************************************************/

StraightLine::StraightLine( double a, double b, double c): a(a), b(b), c(c) {

	confirmCorrectCoords(a,b);
}



/*******************************************************************************
 * Assumes:    -
 * Guarantees: constructs a straight line from two points
 *
 * Throws: invalid_argument if p1 == p2
 ******************************************************************************/

StraightLine::StraightLine(Point p1, Point p2) {

	if (p1 == p2)
		throw std::invalid_argument("StraightLine constructor: "
				                    "distinct points required.");

	if (p2 == 0) std::swap(p1, p2);

	/* p2 != 0*/
	if (p1 == 0) {

		c = 0;
		a = - p2.getY();
		b =   p2.getX();

	}
	/* p1 != 0, p2 != 0*/

	if ( p1.getX() != 0) {

		b = p2.getX() - p1.getX();
		c = p1.getX() * p2.getY() - p1.getY() * p2.getX();
		a = p1.getY() - p2.getY();
	}
	else { /* p1.getY() != 0 */

		a = p2.getY() - p1.getY();
		c = p2.getX() * p1.getY() - p1.getX() * p2.getY();
		b = p1.getX() - p2.getX();

	}


}

/*******************************************************************************
 * Assumes: -
 * Guarantees: "prosta ma przechodzić przez
 * punkt oddalony od początku układu współrzędnych przez zadany wektor
 * i być prostopadła do tego wektora"
 ******************************************************************************/

StraightLine::StraightLine( const Vector &v) {

	Point p(v.getDx(), v.getDy());

	a = v.getDx();
	b = v.getDy();
	c = - (p.getX()*a + p.getY()*b);
}


/*******************************************************************************
 * Assumes: -
 * Guarantees: A newly constructed straight line is the line l translated
 *             by vector v
 *
 * TODO: implementation!
 ******************************************************************************/

StraightLine::StraightLine( const StraightLine &l, const Vector &v) {


}


/******************************************************************************
 *
 * Assumes:    -
 * Guarantees: -
 *
 * Throws: invalid_argument
 *
 * Helper function/
 ******************************************************************************/

inline void StraightLine::confirmCorrectCoords(double a, double b) const {
	if ( (!a) && (!b) )
		throw std::invalid_argument("A and B coordinates of a straight line"
				                    "cannot be 0 at the same time.");
}


/*******************************************************************************
 * Assumes:    -
 * Guarantees: - checks if v and this are orthogonal
 ******************************************************************************/
bool StraightLine::isOrthogonal(const Vector &v) const {

  return v.isScaled({a,b});

}
/*******************************************************************************
 * Assumes:    -
 * Guarantees: - checks if v and this are parallel
 ******************************************************************************/
bool StraightLine::isParallel( const Vector &v) const {

  return v.isScaled({-b,a});
}


bool StraightLine::operator==(const StraightLine &l) const {

	return ( ((a * l.b) == b * l.a ) &&
			 ((b * l.c) == c * l.b ) );

}


/***************************************************************************
 * Assumes:    -
 * Guarantees: - checks if straight line contains a point p
 **************************************************************************/
bool StraightLine::contains( const Point &p) const {

 return  (a*p.getX() + b*p.getY() + c) == 0;

}
