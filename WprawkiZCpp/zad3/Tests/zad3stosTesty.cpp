
#include "zad3stos.hpp"
#include "gtest/gtest.h"

#include<stdexcept>



TEST(Stack, DefaultCtorTest) {

	ASSERT_NO_THROW(Stack<int> sta);


	Stack<int> *st;

	ASSERT_NO_THROW(st = new Stack<int>);

	ASSERT_TRUE(st->isEmpty());
	ASSERT_FALSE(st->isFull());

	ASSERT_NO_THROW(st->Push(1));

	ASSERT_FALSE(st->isEmpty());
	ASSERT_TRUE(st->isFull());

	EXPECT_EQ(st->Top(), 1);

	ASSERT_THROW(st->Push(2), std::runtime_error);

	ASSERT_FALSE(st->isEmpty());
	ASSERT_TRUE(st->isFull());
	EXPECT_EQ(st->Top(), 1);


	ASSERT_NO_THROW(st->Pop());

	ASSERT_TRUE(st->isEmpty());
	ASSERT_FALSE(st->isFull());

	ASSERT_NO_THROW(delete st);

}


const size_t stack_size = 777;

TEST(Stack, ParametrizetCtorTest) {

	Stack<int> *st;

	ASSERT_NO_THROW(st = new Stack<int>(stack_size));

	EXPECT_TRUE(st->isEmpty());

	for(size_t i = 1; i <= stack_size; i++) {
		ASSERT_NO_THROW(st->Push(i));
	}

	EXPECT_TRUE(st->isFull());
    ASSERT_THROW(st->Push(8), std::runtime_error);
    EXPECT_TRUE(st->isFull());

    int popped;

    for(size_t i = stack_size; i > 0; i--) {
    	ASSERT_NO_THROW(st->Top());
    	ASSERT_EQ(st->Top(), i);
        ASSERT_NO_THROW(popped = st->Pop());
    	ASSERT_EQ(popped, i);
    }


    EXPECT_TRUE(st->isEmpty());

	ASSERT_NO_THROW(delete st);

}

TEST(Stack, CopyCtorTest) {


	/* Prepare a stack to be copied. Next time use a test-fixture to
	 * avoid repetition of work.*/

	Stack<int> *st;

	ASSERT_NO_THROW(st = new Stack<int>(stack_size));

	EXPECT_TRUE(st->isEmpty());

	for(size_t i = 1; i <= stack_size; i++) {
	ASSERT_NO_THROW(st->Push(i));
	}

	EXPECT_TRUE(st->isFull());
	ASSERT_THROW(st->Push(8), std::runtime_error);
	EXPECT_TRUE(st->isFull());

	/* Now copy-construct a stack*/

	Stack<int> st_cpy(*st);

	st->Pop(); st->Pop();

	ASSERT_NO_THROW(delete st);

	EXPECT_TRUE(st_cpy.isFull());
    ASSERT_THROW(st_cpy.Push(8), std::runtime_error);
    EXPECT_TRUE(st_cpy.isFull());

    int popped;

    for(size_t i = stack_size; i > 0; i--) {
    	ASSERT_NO_THROW(st_cpy.Top());
    	ASSERT_EQ(st_cpy.Top(), i);
        ASSERT_NO_THROW(popped = st_cpy.Pop());
    	ASSERT_EQ(popped, i);
    }


    EXPECT_TRUE(st_cpy.isEmpty());

}

TEST(Stack, MoveCtorTest) {

	Stack<int> *target;

	{
		Stack<int> source(5);

		source.Push(1);
		source.Push(2);
		source.Push(3);

		target = new Stack<int>(std::move(source));
	}

	EXPECT_EQ(target->Top(),3);


	delete target;
}


