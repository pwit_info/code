//
// Implementation of Stack class
//

#include "zad3stos.hpp"

#include <stdexcept>
#include <iostream>
#include <vector>
#include <initializer_list>

using namespace std;


template class Stack<int>;


////////////////////////////////////////////////////////////////////////////////
// Constructors & assignment operators.
////////////////////////////////////////////////////////////////////////////////

//
// Assumes:    -
// Guarantees: creates stack of capacity 1
//
template<class T>
Stack<T>::Stack() : capacity(1), first_free(0) {

	store = new T[capacity];

}


//
// Assumes:    capacity > 0
// Guarantees: creates stack of a given capacity
// Throws: invalid_argument if capacity == 0
//
template<class T>
Stack<T>::Stack(size_t capacity): capacity(capacity), first_free(0) {

	if (!capacity)
		throw invalid_argument("Stack constructor: capacity must be > 0.");

	store = new T[capacity];
}

//
// Assumes:    -
// Guarantees: Creates a stack of capacity 2d+1, where
//             d is the capacity of list and initializes
//             it with list contents. The rightmost element
//             of list becomes the top of the stack.
//
template<class T>
Stack<T>::Stack(initializer_list<T> list) {

	capacity  = 2*list.size()+1;
	first_free = 0;
	store = new T[capacity];

	for (const auto &e : list) {
		store[first_free++] = e;
	}

}

//
// Copy constructor.
//
template<class T>
Stack<T>::Stack(const Stack<T> &source) {

	capacity = source.capacity;
	first_free = source.first_free;
	store = new T[capacity];

	for( size_t cnt = 0; cnt < first_free; cnt++) {
		store[cnt] = source.store[cnt];
	}
}

//
// Copy assignment operator.
//
template<class T>
Stack<T>& Stack<T>::operator=(const Stack<T> &source) {

//	if (store != nullptr)
//		delete[] store;
//
//	capacity = source.capacity;
//	first_free = source.first_free;
//	store = new T[capacity];
//
//	for( size_t cnt = 0; cnt < first_free; cnt++) {
//		store[cnt] = source.store[cnt];
//	}

	Stack<T> cpy(source);
	*this = move(cpy);

 return *this;
}

//
// Move constructor.
//
template<class T>
Stack<T>::Stack( Stack<T> && rval) {

   capacity = rval.capacity;
   first_free = rval.first_free;
   store = rval.store;

   rval.capacity = 0;
   rval.first_free = 0;
   rval.store = nullptr;

}

//
// Move assignment operator.
//
template<class T>
Stack<T>& Stack<T>::operator=(Stack<T> && rval) {

	swap(rval.capacity, capacity);
	swap(rval.first_free, first_free);
	swap(rval.store, store);

	return *this;
}


////////////////////////////////////////////////////////////////////////////////
// Stack operations.
////////////////////////////////////////////////////////////////////////////////


template<class T>
void Stack<T>::Push(const T &item) {

	if (isFull())
		throw runtime_error("A full stack cannot be pushed onto.");

	store[first_free++] = item;

}

template<class T>
T Stack<T>::Pop(void) {

	if (isEmpty())
		throw runtime_error("An empty stack cannot be popped.");

	return store[--first_free];

}

template<class T>

T Stack<T>::Top(void) const {

	if (isEmpty())
			throw runtime_error("An empty stack has no top element.");

	return store[first_free - 1];
}


//
// Assumes:     -
// Guarantees:  returns the number of actual elements stored on stack
template<class T>
inline size_t Stack<T>::Size(void) const {
	return first_free;
}




