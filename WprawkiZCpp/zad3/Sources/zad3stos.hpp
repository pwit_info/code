//
// Contains declaration of a stack template.
//

#ifndef ZAD3STOS_HPP_
#define ZAD3STOS_HPP_

#include<initializer_list>

//
// Simple stack of limited capacity.
//
// Class invariant: Stacks have capacity >= 1, store is non-null,
//                  first_free points to first available element.
//                  Exception: a moved stack sometimes has a null store,
//                  capacity==0 and first_free==0 (not very elegant).

template <class T>
class Stack {
private:
	std::size_t capacity;
	std::size_t first_free;
	T *store;

public:
	//
	// Assumes:    -
	// Guarantees: creates stack of capacity 1
	//
	Stack();

	//
	// Assumes:    capacity > 0
	// Guarantees: creates stack of a given capacity
	// Throws: invalid_argument if capacity == 0
	//
	Stack(std::size_t);

	//
	// Assumes:    -
	// Guarantees: Creates a stack of capacity 2d+1, where
	//             d is the size of list and initializes
	//             it with list contents. The rightmost element
	//             of list becomes the top of the stack.
	//
	Stack(std::initializer_list<T> list);


	//
	// Copy constructor and assignment operators.
	//
	Stack(const Stack<T> &source);
	Stack<T>& operator=(const Stack<T> &source);

	//
	// Move constructor and move assignment operators.
	//
	Stack( Stack<T> &&);
	Stack& operator=(Stack<T> &&);



	~Stack() { if (store != nullptr) delete[] store;}


	void Push(const T &item);
	T Pop(void);
	T Top(void) const;

	inline std::size_t Size(void) const;

	bool isFull() const {return (first_free >= capacity);}
	bool isEmpty() const {return !isFull() && !first_free;}

};



#endif /* ZAD3STOS_HPP_ */
