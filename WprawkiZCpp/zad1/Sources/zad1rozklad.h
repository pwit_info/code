#ifndef ZAD1ROZKLAD_H_
#define ZAD1ROZKLAD_H_

#include<vector>
#include<iostream>
#include<stdexcept>

namespace zad1rozklad{
using number = long long;
using std::vector;
using std::ostream;


number getDivisor (number n);
vector<number> decompose(number n);
number parseNumber(const char *source, size_t *next);

ostream& operator<<(ostream &os, const vector<number> &v);
}

#endif /* ZAD1ROZKLAD_H_ */


