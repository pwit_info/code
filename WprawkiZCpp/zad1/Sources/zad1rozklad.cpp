/*******************************************************************************
 * Assignment: zad1rozklad.pdf
 *
 * TODO: refactor, add some glue code, add more tests
 *
 * Author: PWit
 * ****************************************************************************/

#include<cmath>
#include<cstring>
#include<limits>
#include "zad1rozklad.h"

#include<string>


namespace zad1rozklad{


/******************************************************************************
 * Assumes:    number is a numerical type that can be promoted to long double
 * Guarantees: Result has the same sign as n.
 * 			   Absolute value of result is the smallest >1 divisor of n.
 *
 *             If n is prime, 1 or -1 then the return value is n.
 ******************************************************************************/
number getDivisor (number n) {

 if (n == std::numeric_limits<number>::min())
	 return -2; //n is even assuming 2's complement representation.

 short sign = n<0?-1:1;

   n *= sign;

 for (number i = 2; i <= sqrt( static_cast<long double>(n)); i++) {

	 if (!(n % i)) return sign*i;
 }

 return sign*n;
}


/******************************************************************************
 * Assumes:    the same as getDivisor(number)
 * Guarantees: Result is a vector of prime divisors of n sorted
 * 			   in non-descending order. First element of the result
 * 			   has the same sign as n. Remaining elements are positive.
 *
 ******************************************************************************/
vector<number> decompose(number n)
{

 	vector<number> result;

 	number divisor;


 	if (n == -1 || n == 0 || n == 1)
 		return {n};


 	while (n != 1) {

      divisor = getDivisor(n);
      result.push_back(divisor);
      n /= divisor;
 	}

	return result;
}


unsigned int integerLog10(number n); // function def. appears at the bottom
									 // of the file




/*******************************************************************************
 * Assumes: source is a C string, next is an initialized pointer to size_t
 * Guarantees: if the number obtained from source after skipping whitespaces
 *             fits in type number then the function returns it. Otherwise it
 *             raises an appropriate exception.
 *
 * TODO: refactoring
 ******************************************************************************/

number parseNumber(const char *source, size_t *next) {

	size_t start = 0;

	/* skip white characters*/
	while ( (source[start] != '\n') && isblank(source[start]))
		start++;

	/* expect source[start] to be a digit or '-' sign*/
	if ((source[start] == '\n') || (!isdigit(source[start]) && (source[start]!= '-')) )
		throw std::invalid_argument("parseNumber error: no integer in string");

	short sign = source[start] == '-'? -1 : 1;
	if ( sign == -1) {
		start++;
		if ((source[start] == '\n') || !isdigit(source[start]) )
				throw std::invalid_argument("parseNumber error: no integer in string");

	}

	/*find the end of the number*/
	size_t oneAfterEnd = start;

	while ( (source[oneAfterEnd] != '\n') && isdigit(source[oneAfterEnd]))
		oneAfterEnd++;


	number max_or_min;

	if (sign == 1) {

		max_or_min = std::numeric_limits<number>::max();

	} else { /* sign == -1*/

		max_or_min = std::numeric_limits<number>::min();
	}


	/*expect the number to be converted to fit in type number*/
	if (oneAfterEnd - start >  integerLog10(max_or_min) )
		throw std::invalid_argument("parseNumber error: integer over- or underflow");

	if (oneAfterEnd - start ==  integerLog10(max_or_min) ) {
	/*the integer to be converted may still be out of type's number range*/
	/*here we only detect a possible overflow*/

		size_t index = oneAfterEnd;

		do {
			index--;
			if (source[index] - '0' > sign*(max_or_min % 10))
				throw std::invalid_argument("parseNumber error: integer over- or underflow");
			max_or_min /= 10;
		} while (index > start);

	}

	/*no overflow will occur*/
	/**/

	size_t index = start;
	number result = 0;


	while (index < oneAfterEnd) {

		result = 10*result + (source[index] - '0') ;
		index++;
	}


	*next = oneAfterEnd;
	return sign*result;
}





/******************************************************************************
 * Assumes: -
 * Guarantees: -
 *
 *  A helper function that prints vector of numbers.
 ******************************************************************************/
std::ostream& operator<<(std::ostream &os, const vector<number> &v) {

	for(auto &e : v)
     std::cout << e << " ";

		return os;
}



/******************************************************************************
 * Assumes:    -
 * Guarantees: result is the number of decimal digits sufficient and necessary
 *             to store n
 *
 * Note: we cannot use built-in cmath functions as they require n > 0. We cannot
 * just run them on -n as -n may be outside of type's number range.
 *
 ******************************************************************************/

unsigned int integerLog10(number n) {


	unsigned int result = 0;

	while (n) {

		n /= 10;
		result++;
	}

	return result;
}


} /*end of namespace zad1rozklad*/



















