/*
 * zad1rozkladTesty.cpp
 *
 *  Created on: 27 mar 2016
 *      Author: pwit
 */

#include "zad1rozklad.h"

#include "gtest/gtest.h"


#include<limits>

using namespace zad1rozklad;

TEST(getDivisorTest, SizeAssumptions) {

	ASSERT_LT(sizeof(number), sizeof(long double));

}

TEST(getDivisorTest, divisorsOfAPrime) {

	EXPECT_EQ(1, getDivisor(1));
	EXPECT_EQ(2, getDivisor(2));
	EXPECT_EQ(3, getDivisor(3));
	EXPECT_EQ(5, getDivisor(5));

}

TEST(getDivisorTest, divisorsOfANegativePrime) {

	EXPECT_EQ(-1, getDivisor(-1));
	EXPECT_EQ(-2, getDivisor(-2));
	EXPECT_EQ(-3, getDivisor(-3));
	EXPECT_EQ(-5, getDivisor(-5));
}


TEST(getDivisorTest, divisorsOfNonPrimes) {

	EXPECT_EQ(0, getDivisor(0));
	EXPECT_EQ(2, getDivisor(4));
	EXPECT_EQ(2, getDivisor(6));
	EXPECT_EQ(2, getDivisor(8));
	EXPECT_EQ(3, getDivisor(9));
	EXPECT_EQ(3, getDivisor(15));
}

TEST(getDivisorTest, divisorsOfNegativeNonPrimes) {

	EXPECT_EQ(-2, getDivisor(-4));
	EXPECT_EQ(-2, getDivisor(-6));
	EXPECT_EQ(-2, getDivisor(-8));
	EXPECT_EQ(-3, getDivisor(-9));
	EXPECT_EQ(-3, getDivisor(-15));
}


#include<vector>

using namespace std;


TEST(decomposeTests, positives) {

	vector<number> v0 = {0};
	EXPECT_EQ(v0, decompose(0));

	vector<number> v1 = {1};
	EXPECT_EQ(v1, decompose(1));

	vector<number> v2 = {3};
	EXPECT_EQ(v2, decompose(3));

	vector<number> v3 = {2,3};
	EXPECT_EQ(v3, decompose(6));

	vector<number> v4 = {2,3,5};
	EXPECT_EQ(v4, decompose(30));


}

TEST(decomposeTests, negatives) {

	vector<number> v1 = {-1};
	EXPECT_EQ(v1, decompose(-1));

	vector<number> v2 = {-2};
	EXPECT_EQ(v2, decompose(-2));

	vector<number> v3 = {-3};
	EXPECT_EQ(v3, decompose(-3));

	vector<number> v4 = {-2,3};
	EXPECT_EQ(v4, decompose(-6));

	vector<number> v5 = {-2,3,5};
	EXPECT_EQ(v5, decompose(-30));

}



TEST(decomposeTest, BigPrime) { /*Takes a long time to complete.*/

	ASSERT_EQ(sizeof(long long),8) << "This test is only valid for long long being 8 bytes long";


	vector<number> v5 = {9223372036854775783};

	/*Costly test commented out:*/
	//EXPECT_EQ(v5,decompose(9223372036854775783));



}

TEST(decomposeTest, Max) {

	ASSERT_EQ(sizeof(long long),8) << "This test is only valid for long long being 8 bytes long";


	ASSERT_EQ(7, getDivisor(7));
	ASSERT_EQ(73, getDivisor(73));
	ASSERT_EQ(127, getDivisor(127));
	ASSERT_EQ(337, getDivisor(337));
	ASSERT_EQ(92737, getDivisor(92737));
	ASSERT_EQ(649657, getDivisor(649657));

	long long expected_max = 7*7*73*127*337;
	    	  expected_max *= 92737;
	    	  expected_max *= 649657;


	EXPECT_EQ(expected_max,std::numeric_limits<long long>::max());



	vector<number> v6 = {7,7,73,127,337,92737,649657};
	EXPECT_EQ(v6,decompose(std::numeric_limits<long long>::max()));


}


TEST(decomposeTest, Min) {

	ASSERT_EQ(sizeof(long long),8) << "This test is only valid for long long being 8 bytes long";

	vector<number> v6(63);

	for (auto &val : v6) {
		val = 2;
	}

	v6[0] = -2;

	EXPECT_EQ(v6,decompose(std::numeric_limits<long long>::min()));


}



TEST(parseNumberTest, SimpleNaturals) {

	struct { const char *str;
			 number value;
		     size_t next;}  test_table[] =
    {

    {"0", 0, 1}, {" 0", 0, 2}, {" 0 ", 0, 2},
	{"12345", 12345, 5}, {"12345 55", 12345, 5}, {"1",1,1}, {"12", 12, 2},
	{"1 fd11223433dd", 1, 1}, {"0 xdw", 0, 1}, {"     123", 123, 8},
	{"9223372036854775807", std::numeric_limits<number>::max(), 19},
	{"   9223372036854775807", std::numeric_limits<number>::max(), 22},
	{"   9223372036854775807dffe", std::numeric_limits<number>::max(), 22}

	};


	size_t next;

	for( auto &ent: test_table) {

		EXPECT_EQ( parseNumber(ent.str, &next), ent.value);
		EXPECT_EQ( ent.next, next);
	}

}

TEST(parseNumberTest, SimpleNegatives) {

	struct { const char *str;
			 number value;
		     size_t next;}  test_table[] =
    {
    	{"-1", -1, 2}, {"-1ddd ff", -1, 2}, {" -1", -1, 3},
	{"-12345", -12345, 6}, {"-12345 55", -12345, 6}, {"-111",-111,4}, {"-12", -12, 3},
	{"-1 fd11223433dd", -1, 2}, {"     -123", -123, 9},
	{"-9223372036854775808", std::numeric_limits<number>::min(), 20},
	{"   -9223372036854775808", std::numeric_limits<number>::min(), 23},
	{"   -9223372036854775808dffe", std::numeric_limits<number>::min(), 23}

	};


	size_t next;

	for( auto &ent: test_table) {

		EXPECT_EQ( parseNumber(ent.str, &next), ent.value);
		EXPECT_EQ( ent.next, next);
	}

}




